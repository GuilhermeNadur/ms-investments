# Microservice Investments - RESTful API (Desafio Banco Inter 🧡)

## 📝 Descrição
Esta aplicação foi criada para atender o desafio técnico proposto pelo Banco Inter. Seu objetivo é disponibilizar um microserviço com as seguintes funcionalidades:
- Leitura, cadastro, atualização (completa e somente de status) e deleção de empresas.
- Leitura, cadastro, atualização e deleção de clientes;
- Leitura e cadastro de investimentos no qual o sistema distribua o dinheiro da forma mais homogênea e eficiente possível entre as empresas com status ativo, de modo a dar o menor troco possível para o cliente.

## 🤖 Tecnologias
- Java [17.0.4.1]
- Maven [3.8.6]
- Spring Boot [2.7.3] (MVC, JPA, HATEOAS, JUnit5, Cache, Actuator & Validation)
- H2 Database [2.1.214]
- Lombok [1.18.24]
- Mapstruct [1.5.2.Final]
- OpenAPI [1.6.11]

## ✅ Requisitos
- [JDK 17.0.4.1](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html)
- [Maven 3.8.6](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html)
- [IntelliJ IDEA (recomendado)](https://www.jetbrains.com/idea/download)
- [Postman (recomendado)](https://www.postman.com/downloads/)
- [Git (recomendado)](https://git-scm.com/downloads)

## 🖥️ Como Rodar
Execute o seguinte comando na raiz do projeto para executar os testes unitários:
```bash
mvn test
```
Execute o seguinte comando na raiz do projeto para executar o Spring Boot:
```bash
mvn spring-boot:run
```

## 🧠 Lógica e Algorítmo
Para realizar uma requisição de investimento, é necessário atender primeiramente os seguintes requisitos:
- Deve ser enviado um CPF válido.
- O valor a ser investido deve ser superior a R$ 1,00.
- A quantidade de empresas a diversificar o investimento deve ser superior a 1.
- O cliente deve estar cadastrado no sistema.
- Ter ao menos uma empresa cadastrada e ativa no sistema.

Sendo atendido esses primeiros requisitos, o sistema partirá para próxima etapa. A partir do total a investir informado, a aplicação irá verificar se existe alguma empresa ativa com valor de ação igual ou menor a este total. Caso não exista nenhuma empresa possível a investir, o sistema interromperá o fluxo nesta etapa, mas caso exista empresas com este requisito, o sistema partirá para a próxima etapa.

Tendo em mente que neste momento a aplicação possui uma lista de possíveis empresas a investir, o sistema começará a escolher em quais empresas serão investidas. Para executar essa lógica, a aplicação irá separar essa lista de possíveis empresas em duas. A primeira lista conterá as empresas da qual o cliente nunca investiu e a segunda conterá as empresas da qual o cliente já investiu ao menos uma vez.

A partir desse momento, algumas possibilidades podem acontecer. São elas:
1. A primeira lista possui de 1-N empresas e a segunda está vazia, ou seja, o cliente ainda não investiu em nenhuma das possíveis empresas coletadas.
2. A primeira lista está vazia e a segunda possui de 1-N empresas, ou seja, o cliente já investiu em todas das possíveis empresas coletadas.
3. Ambas as listas possuem de 1-N empresas, ou seja, o cliente já investiu em algumas das possíveis empresas e em outras não.

Observação: as listas estarão ordenadas de forma decrescente levando em consideração o valor da ação de cada empresa, ou seja, o primeiro elemento possui o valor de ação mais alto e o último elemento possui o valor de ação mais baixo.

Depois de separarmos as empresas em duas listas, o sistema irá definir qual vai ser a estratégia de diversificação. Para que isso seja possível, foram criadas duas estratégias. São elas:
1. Diversificador Padrão: escolhe as empresas que estão nas posições "LAST, MIDDLE e FIRST" (última, do meio e primeira), ou seja, menor valor de ação, valor do meio, maior valor de ação.
2. Diversificador Semi-Randômico: escolhe as empresas que estão nas posições "RANDOM, LAST, RANDOM, MIDDLE, RANDOM e FIRST". A opção "RANDOM" obterá uma empresa aleatória da lista.

Observação: os diversificadores possuem um ciclo infinito (na linguagem Java chamamos de Cycle Iterator).

Agora que entendemos quais estratégias podem ser utilizadas, vamos entender em quais momentos elas entram em ação. Para isso, vamos levar em consideração as 3 possibilidades citadas anteriormente quando as listas foram divididas em duas:
1. Se a opção 1 for a atual (cliente ainda não investiu em nenhuma das possíveis empresas coletadas), o sistema iniciará as escolhas utilizando o Diversificador Padrão coletando as empresas que nunca foram investidas pelo cliente. Este processo só finalizará caso o número de empresas a diversificar informado pelo cliente for alcançado ou ambas as listas ficarem vazias.
2. Se a opção 2 for a atual (cliente já investiu em todas das possíveis empresas coletadas), o sistema iniciará as escolhas utilizando o Diversificador Semi-Randômico coletando as empresas que já foram investidas pelo cliente. O processo também finalizará seguindo as regras mencionadas na opção 1.
3. Se a opção 3 for a atual (cliente já investiu em algumas das possíveis empresas e em outras não), o sistema iniciará as escolhas utilizando o Diversificador Padrão coletando as empresas que nunca foram investidas pelo cliente. Caso essa lista venha de esvaziar e ainda não foi atingido o número de empresas a diversificar informado pelo cliente, o sistema trocará para o Diversificador Semi-Randômico e irá continuar escolhendo as empresas que já foram investidas pelo cliente. O processo também finalizará seguindo as regras mencionadas na opção 1.

Tendo em vista que nesse ponto do processo já teremos as empresas escolhidas, o sistema iniciará a etapa de efetivar o investimento nas empresas.

Observação: a lista de empresas escolhidas estará ordenada conforme a adição foi feita, ou seja, primeiro as empresas que ainda não foram investidas (se existir) seguindo a estratégia do Diversificador Padrão, depois as empresas que já tiveram investimentos feitos (se existir) seguindo a estratégia do Diversificador Semi-Randômico.

Ao começar a efetivação dos investimentos, o total informado será levado em consideração. O sistema irá iniciar os investimentos pegando a primeira empresa escolhida da lista e irá até a última, realizando um investimento em cada uma. Caso tenha chegado no último elemento, o sistema irá iniciar novamente a partir do primeiro elemento. Este fluxo só irá finalizar até que o total a investir seja menor que o valor de ação mais baixa dentre as empresas escolhidas, ou seja, quando não for mais possível investir. Caso tenha sobrado algum valor, ele será atribuído como troco.

Por fim, a lista de investimentos realizados será salva no banco de dados e as informações serão retornadas ao cliente. Este processo completo costuma levar cerca de 400-700 milissegundos.

## 💭 Entidade e Relacionamento (ER)
<img alt="Diagrama ER Microservice Investments png" height="496" src="https://uploaddeimagens.com.br/images/004/026/072/full/Diagrama_ER_-_Microservice_Investments.png?1663544823" width="590"/>

## 💾 Banco de Dados
Ao iniciar a aplicação, o Spring Boot irá executar o arquivo "data.sql" (localizado em "src/main/resources/") automaticamente. Dessa forma, as tabelas "tb_company" e "tb_customer" serão alimentadas com o script SQL abaixo:
```sql
INSERT INTO tb_company (name, ticker, stock_price, status)
VALUES ('Banco Inter', 'BIDI11', 66.51, true),
    ('Magazine Luiza', 'MGLU3', 18.80, true),
    ('Sul América', 'SULA11', 28.26, true),
    ('Engie', 'EGIE3', 38.30, true),
    ('CVC Brasil', 'CVCB3', 20.87, true),
    ('Lojas Renner', 'LREN3', 36.95, true),
    ('Marisa', 'AMAR3', 6.30, true);

INSERT INTO tb_customer (name, cpf)
VALUES ('Guilherme Rodrigues Nadur', '467.589.878-50');
```
O H2 Database pode ser acessado através do seguinte caminho:
```
http://localhost:8080/h2-console
```
> Observação: utilize "jdbc:h2:mem:db_ms_investments" como JDBC URL para se conectar.

Queries auxiliares:
```sql
SELECT *
FROM tb_company
ORDER BY stock_price DESC;

SELECT *
FROM tb_customer;

SELECT *
FROM tb_investment;

SELECT DISTINCT(customer_id), company_id, SUM(total), SUM(number_shares), SUM(change)
FROM tb_investment
GROUP BY customer_id, company_id;
```

## 👩‍🚀 Postman
A biblioteca do Postman foi exportada para raiz do projeto. O arquivo é o "MS Investments (Banco Inter).postman_collection.json" e o caminho é "ms-investments/postman".

## 📊 Spring Actuator
O Actuator foi configurado para rodar na porta 8081. Acesse os componentes através dos seguintes caminhos:
```
http://localhost:8081/actuator
```
```
http://localhost:8081/actuator/health
```
```
http://localhost:8081/actuator/swagger-ui
```
```
http://localhost:8081/actuator/openapi
```