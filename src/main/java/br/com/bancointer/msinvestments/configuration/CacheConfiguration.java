package br.com.bancointer.msinvestments.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableCaching
@Configuration
public class CacheConfiguration {

    @Value("${application.cache.names}")
    private String[] applicationCacheNames;

    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager(applicationCacheNames);
    }
}
