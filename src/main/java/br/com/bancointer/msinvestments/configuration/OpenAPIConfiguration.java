package br.com.bancointer.msinvestments.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenAPIConfiguration {

    @Bean
    public OpenAPI microserviceInvestmentsOpenAPI() {
        return new OpenAPI()
                .info(new Info()
                        .title("Microservice Investments - RESTful API")
                        .description("Desafio Técnico - Banco Inter")
                        .version("v1.0.0-SNAPSHOT")
                        .license(new License()
                                .name("Apache 2.0")
                                .url("https://www.apache.org/licenses/LICENSE-2.0")));
    }
}
