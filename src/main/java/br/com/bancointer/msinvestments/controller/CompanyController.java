package br.com.bancointer.msinvestments.controller;

import br.com.bancointer.msinvestments.controller.documentation.CompanyControllerDocumentation;
import br.com.bancointer.msinvestments.domain.Company;
import br.com.bancointer.msinvestments.domain.dto.CompanyDTO;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyConflictDetectedException;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyNotFoundException;
import br.com.bancointer.msinvestments.service.CompanyService;
import br.com.bancointer.msinvestments.util.MessageUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/api/v1/company")
@CrossOrigin(origins = "*", maxAge = 3600)
@AllArgsConstructor
public class CompanyController implements CompanyControllerDocumentation {

    private final CompanyService companyService;

    @Caching(evict = {
            @CacheEvict(cacheNames = "companies_using_key_pageable_to_string", allEntries = true),
            @CacheEvict(cacheNames = "companies_active_using_key_pageable_to_string", allEntries = true)
    })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> saveCompany(@RequestBody @Valid CompanyDTO companyDTO) throws CompanyNotFoundException, CompanyConflictDetectedException {
        log.info("Saving the company [{}]", companyDTO.getName());
        return ResponseEntity.status(HttpStatus.CREATED).body(companyService.save(companyDTO));
    }

    @Cacheable(cacheNames = "companies_using_key_pageable_to_string", key = "#pageable.toString()")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<Company>> getAllCompanies(@PageableDefault(sort = "id", direction = Sort.Direction.ASC) Pageable pageable) throws CompanyNotFoundException {
        log.info("Getting all companies...");
        return ResponseEntity.status(HttpStatus.OK).body(companyService.findAll(pageable));
    }

    @Cacheable(cacheNames = "companies_active_using_key_pageable_to_string", key = "#pageable.toString()")
    @GetMapping(value = "/active", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<Company>> getAllActiveCompanies(@PageableDefault(sort = "id", direction = Sort.Direction.ASC) Pageable pageable) throws CompanyNotFoundException {
        log.info("Getting all active companies...");
        return ResponseEntity.status(HttpStatus.OK).body(companyService.findAllByStatus(pageable, Boolean.TRUE));
    }

    @Cacheable(cacheNames = "companies_using_key_id", key = "#id")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getOneCompany(@PathVariable(value = "id") Long id) throws CompanyNotFoundException {
        log.info("Getting the company with ID [{}]", id);
        return ResponseEntity.status(HttpStatus.OK).body(companyService.findById(id));
    }

    @Caching(put = @CachePut(cacheNames = "companies_using_key_id", key = "#id"),
            evict = {
                @CacheEvict(cacheNames = "companies_using_key_pageable_to_string", allEntries = true),
                @CacheEvict(cacheNames = "companies_active_using_key_pageable_to_string", allEntries = true)
            })
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateCompany(@PathVariable(value = "id") Long id, @RequestBody @Valid CompanyDTO companyDTO) throws CompanyNotFoundException {
        log.info("Updating the company with ID [{}]", id);
        return ResponseEntity.status(HttpStatus.OK).body(companyService.update(id, companyDTO));
    }

    @Caching(put = @CachePut(cacheNames = "companies_using_key_id", key = "#id"),
            evict = {
                @CacheEvict(cacheNames = "companies_using_key_pageable_to_string", allEntries = true),
                @CacheEvict(cacheNames = "companies_active_using_key_pageable_to_string", allEntries = true)
    })
    @PatchMapping(value = "/{id}/active", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateCompanyToActive(@PathVariable(value = "id") Long id) throws CompanyNotFoundException {
        log.info("Updating the company with ID [{}] to active status", id);
        return ResponseEntity.status(HttpStatus.OK).body(companyService.updateToActive(id));
    }

    @Caching(put = @CachePut(cacheNames = "companies_using_key_id", key = "#id"),
            evict = {
                @CacheEvict(cacheNames = "companies_using_key_pageable_to_string", allEntries = true),
                @CacheEvict(cacheNames = "companies_active_using_key_pageable_to_string", allEntries = true)
    })
    @PatchMapping(value = "/{id}/inactive", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateCompanyToInactive(@PathVariable(value = "id") Long id) throws CompanyNotFoundException {
        log.info("Updating the company with ID [{}] to inactive status", id);
        return ResponseEntity.status(HttpStatus.OK).body(companyService.updateToInactive(id));
    }

    @Caching(evict = {
            @CacheEvict(cacheNames = "companies_using_key_id", key = "#id"),
            @CacheEvict(cacheNames = "companies_using_key_pageable_to_string", allEntries = true),
            @CacheEvict(cacheNames = "companies_active_using_key_pageable_to_string", allEntries = true)
    })
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteCompany(@PathVariable(value = "id") Long id) throws CompanyNotFoundException {
        log.info("Deleting the company with ID [{}]", id);
        companyService.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).body(MessageUtils.getDeleteSuccessMessageDTO(Company.class.getSimpleName(), id));
    }
}
