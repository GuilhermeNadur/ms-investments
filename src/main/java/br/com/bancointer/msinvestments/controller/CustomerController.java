package br.com.bancointer.msinvestments.controller;

import br.com.bancointer.msinvestments.controller.documentation.CustomerControllerDocumentation;
import br.com.bancointer.msinvestments.domain.Customer;
import br.com.bancointer.msinvestments.domain.dto.CustomerDTO;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerConflictDetectedException;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerNotFoundException;
import br.com.bancointer.msinvestments.service.CustomerService;
import br.com.bancointer.msinvestments.util.MessageUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/api/v1/customer")
@CrossOrigin(origins = "*", maxAge = 3600)
@AllArgsConstructor
public class CustomerController implements CustomerControllerDocumentation {

    private final CustomerService customerService;

    @CacheEvict(cacheNames = "customers_using_key_pageable_to_string", allEntries = true)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> saveCustomer(@RequestBody @Valid CustomerDTO customerDTO) throws CustomerNotFoundException, CustomerConflictDetectedException {
        log.info("Saving the customer [{}]", customerDTO.getName());
        return ResponseEntity.status(HttpStatus.CREATED).body(customerService.save(customerDTO));
    }

    @Cacheable(cacheNames = "customers_using_key_pageable_to_string", key = "#pageable.toString()")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<Customer>> getAllCustomers(@PageableDefault(sort = "id", direction = Sort.Direction.ASC) Pageable pageable) throws CustomerNotFoundException {
        log.info("Getting all customers...");
        return ResponseEntity.status(HttpStatus.OK).body(customerService.findAll(pageable));
    }

    @Cacheable(cacheNames = "customers_using_key_id", key = "#id")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getOneCustomer(@PathVariable(value = "id") Long id) throws CustomerNotFoundException {
        log.info("Getting the customer with ID [{}]", id);
        return ResponseEntity.status(HttpStatus.OK).body(customerService.findById(id));
    }

    @Caching(put = @CachePut(cacheNames = "customers_using_key_id", key = "#id"),
            evict = @CacheEvict(cacheNames = "customers_using_key_pageable_to_string", allEntries = true))
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateCustomer(@PathVariable(value = "id") Long id, @RequestBody @Valid CustomerDTO customerDTO) throws CustomerNotFoundException {
        log.info("Updating the customer with ID [{}]", id);
        return ResponseEntity.status(HttpStatus.OK).body(customerService.update(id, customerDTO));
    }

    @Caching(evict = {
            @CacheEvict(cacheNames = "customers_using_key_id", key = "#id"),
            @CacheEvict(cacheNames = "customers_using_key_pageable_to_string", allEntries = true)
    })
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteCustomer(@PathVariable(value = "id") Long id) throws CustomerNotFoundException {
        log.info("Deleting the customer with ID [{}]", id);
        customerService.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).body(MessageUtils.getDeleteSuccessMessageDTO(Customer.class.getSimpleName(), id));
    }
}
