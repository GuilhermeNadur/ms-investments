package br.com.bancointer.msinvestments.controller;

import br.com.bancointer.msinvestments.controller.documentation.InvestmentControllerDocumentation;
import br.com.bancointer.msinvestments.domain.Investment;
import br.com.bancointer.msinvestments.domain.dto.request.InvestmentRequest;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyNotFoundException;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerNotFoundException;
import br.com.bancointer.msinvestments.domain.exception.investment.InsuficientTotalToInvestException;
import br.com.bancointer.msinvestments.domain.exception.investment.InvestmentNotFoundException;
import br.com.bancointer.msinvestments.service.InvestmentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/api/v1/investment")
@CrossOrigin(origins = "*", maxAge = 3600)
@AllArgsConstructor
public class InvestmentController implements InvestmentControllerDocumentation {

    private final InvestmentService investmentService;

    @CacheEvict(cacheNames = "investments_using_key_pageable_to_string_customer_id_company_id", allEntries = true)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> saveInvestment(@RequestBody @Valid InvestmentRequest investmentRequest) throws CompanyNotFoundException, InvestmentNotFoundException, CustomerNotFoundException, InsuficientTotalToInvestException {
        log.info("Making the investment for customer with CPF: [{}]", investmentRequest.getCustomerCpf());
        return ResponseEntity.status(HttpStatus.CREATED).body(investmentService.save(investmentRequest));
    }

    @Cacheable(cacheNames = "investments_using_key_pageable_to_string_customer_id_company_id", key = "{#pageable.toString(), #customerId, #companyId}")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<Investment>> getAllInvestments(@PageableDefault(sort = "id", direction = Sort.Direction.ASC) Pageable pageable,
                                                              @RequestParam(value = "customerId", required = false) Long customerId,
                                                              @RequestParam(value = "companyId", required = false) Long companyId) throws InvestmentNotFoundException {
        log.info("Getting all investments by customerId [{}] and companyId [{}]", customerId, companyId);
        return ResponseEntity.status(HttpStatus.OK).body(investmentService.findAllByCustomerIdAndCompanyId(pageable, customerId, companyId));
    }

    @Cacheable(cacheNames = "investments_using_key_id", key = "#id")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getOneInvestment(@PathVariable(value = "id") Long id) throws InvestmentNotFoundException {
        log.info("Getting the investment with ID [{}]", id);
        return ResponseEntity.status(HttpStatus.OK).body(investmentService.findById(id));
    }
}
