package br.com.bancointer.msinvestments.controller.documentation;

import br.com.bancointer.msinvestments.domain.Company;
import br.com.bancointer.msinvestments.domain.dto.CompanyDTO;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyConflictDetectedException;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyNotFoundException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

@Tag(name = "Company")
public interface CompanyControllerDocumentation {

    @Operation(summary = "Save a company")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Quando a empresa é CRIADA (When the company was CREATED)"),
            @ApiResponse(responseCode = "400", description = "Quando o cliente envia um REQUISIÇÃO INCORRETA (When the client sends a BAD REQUEST)"),
            @ApiResponse(responseCode = "409", description = "Quando um CONFLITO é detectado (When a CONFLICT is detected)"),
            @ApiResponse(responseCode = "500", description = "Quando ocorre um ERRO INTERNO NO SERVIDOR (When a INTERNAL SERVER ERROR occurs)")
    })
    ResponseEntity<Object> saveCompany(CompanyDTO companyDTO) throws CompanyNotFoundException, CompanyConflictDetectedException;

    @Operation(summary = "Get all companies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quando a requisição foi OK (When the request was OK)"),
            @ApiResponse(responseCode = "400", description = "Quando o cliente envia um REQUISIÇÃO INCORRETA (When the client sends a BAD REQUEST)"),
            @ApiResponse(responseCode = "404", description = "Quando a empresa NÃO FOI ENCONTRADA (When the company was NOT FOUND)"),
            @ApiResponse(responseCode = "500", description = "Quando ocorre um ERRO INTERNO NO SERVIDOR (When a INTERNAL SERVER ERROR occurs)")
    })
    ResponseEntity<Page<Company>> getAllCompanies(@ParameterObject Pageable pageable) throws CompanyNotFoundException;

    @Operation(summary = "Get all active companies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quando a requisição foi OK (When the request was OK)"),
            @ApiResponse(responseCode = "400", description = "Quando o cliente envia um REQUISIÇÃO INCORRETA (When the client sends a BAD REQUEST)"),
            @ApiResponse(responseCode = "404", description = "Quando a empresa NÃO FOI ENCONTRADA (When the company was NOT FOUND)"),
            @ApiResponse(responseCode = "500", description = "Quando ocorre um ERRO INTERNO NO SERVIDOR (When a INTERNAL SERVER ERROR occurs)")
    })
    ResponseEntity<Page<Company>> getAllActiveCompanies(@ParameterObject Pageable pageable) throws CompanyNotFoundException;

    @Operation(summary = "Get one company")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quando a requisição foi OK (When the request was OK)"),
            @ApiResponse(responseCode = "404", description = "Quando a empresa NÃO FOI ENCONTRADA (When the company was NOT FOUND)"),
            @ApiResponse(responseCode = "500", description = "Quando ocorre um ERRO INTERNO NO SERVIDOR (When a INTERNAL SERVER ERROR occurs)")
    })
    ResponseEntity<Object> getOneCompany(Long id) throws CompanyNotFoundException;

    @Operation(summary = "Update a company")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quando a requisição foi OK (When the request was OK)"),
            @ApiResponse(responseCode = "404", description = "Quando a empresa NÃO FOI ENCONTRADA (When the company was NOT FOUND)"),
            @ApiResponse(responseCode = "500", description = "Quando ocorre um ERRO INTERNO NO SERVIDOR (When a INTERNAL SERVER ERROR occurs)")
    })
    ResponseEntity<Object> updateCompany(Long id, CompanyDTO companyDTO) throws CompanyNotFoundException;

    @Operation(summary = "Update a company to active status")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quando a requisição foi OK (When the request was OK)"),
            @ApiResponse(responseCode = "404", description = "Quando a empresa NÃO FOI ENCONTRADA (When the company was NOT FOUND)"),
            @ApiResponse(responseCode = "500", description = "Quando ocorre um ERRO INTERNO NO SERVIDOR (When a INTERNAL SERVER ERROR occurs)")
    })
    ResponseEntity<Object> updateCompanyToActive(Long id) throws CompanyNotFoundException;

    @Operation(summary = "Update a company to inactive status")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quando a requisição foi OK (When the request was OK)"),
            @ApiResponse(responseCode = "404", description = "Quando a empresa NÃO FOI ENCONTRADA (When the company was NOT FOUND)"),
            @ApiResponse(responseCode = "500", description = "Quando ocorre um ERRO INTERNO NO SERVIDOR (When a INTERNAL SERVER ERROR occurs)")
    })
    ResponseEntity<Object> updateCompanyToInactive(Long id) throws CompanyNotFoundException;

    @Operation(summary = "Delete a company")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quando a requisição foi OK (When the request was OK)"),
            @ApiResponse(responseCode = "404", description = "Quando a empresa NÃO FOI ENCONTRADA (When the company was NOT FOUND)"),
            @ApiResponse(responseCode = "500", description = "Quando ocorre um ERRO INTERNO NO SERVIDOR (When a INTERNAL SERVER ERROR occurs)")
    })
    ResponseEntity<Object> deleteCompany(Long id) throws CompanyNotFoundException;
}
