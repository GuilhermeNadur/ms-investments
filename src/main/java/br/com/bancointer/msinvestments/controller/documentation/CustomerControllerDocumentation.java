package br.com.bancointer.msinvestments.controller.documentation;

import br.com.bancointer.msinvestments.domain.Customer;
import br.com.bancointer.msinvestments.domain.dto.CustomerDTO;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerConflictDetectedException;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerNotFoundException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

@Tag(name = "Customer")
public interface CustomerControllerDocumentation {

    @Operation(summary = "Save a customer")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Quando a cliente é CRIADO (When the customer was CREATED)"),
            @ApiResponse(responseCode = "400", description = "Quando o cliente envia um REQUISIÇÃO INCORRETA (When the client sends a BAD REQUEST)"),
            @ApiResponse(responseCode = "409", description = "Quando um CONFLITO é detectado (When a CONFLICT is detected)"),
            @ApiResponse(responseCode = "500", description = "Quando ocorre um ERRO INTERNO NO SERVIDOR (When a INTERNAL SERVER ERROR occurs)")
    })
    ResponseEntity<Object> saveCustomer(CustomerDTO customerDTO) throws CustomerNotFoundException, CustomerConflictDetectedException;

    @Operation(summary = "Get all customers")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quando a requisição foi OK (When the request was OK)"),
            @ApiResponse(responseCode = "400", description = "Quando o cliente envia um REQUISIÇÃO INCORRETA (When the client sends a BAD REQUEST)"),
            @ApiResponse(responseCode = "404", description = "Quando a cliente NÃO FOI ENCONTRADO (When the customer was NOT FOUND)"),
            @ApiResponse(responseCode = "500", description = "Quando ocorre um ERRO INTERNO NO SERVIDOR (When a INTERNAL SERVER ERROR occurs)")
    })
    ResponseEntity<Page<Customer>> getAllCustomers(@ParameterObject Pageable pageable) throws CustomerNotFoundException;

    @Operation(summary = "Get one customer")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quando a requisição foi OK (When the request was OK)"),
            @ApiResponse(responseCode = "404", description = "Quando a cliente NÃO FOI ENCONTRADO (When the customer was NOT FOUND)"),
            @ApiResponse(responseCode = "500", description = "Quando ocorre um ERRO INTERNO NO SERVIDOR (When a INTERNAL SERVER ERROR occurs)")
    })
    ResponseEntity<Object> getOneCustomer(Long id) throws CustomerNotFoundException;

    @Operation(summary = "Update a customer")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quando a requisição foi OK (When the request was OK)"),
            @ApiResponse(responseCode = "404", description = "Quando a cliente NÃO FOI ENCONTRADO (When the customer was NOT FOUND)"),
            @ApiResponse(responseCode = "500", description = "Quando ocorre um ERRO INTERNO NO SERVIDOR (When a INTERNAL SERVER ERROR occurs)")
    })
    ResponseEntity<Object> updateCustomer(Long id, CustomerDTO customerDTO) throws CustomerNotFoundException;

    @Operation(summary = "Delete a customer")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quando a requisição foi OK (When the request was OK)"),
            @ApiResponse(responseCode = "404", description = "Quando a cliente NÃO FOI ENCONTRADO (When the customer was NOT FOUND)"),
            @ApiResponse(responseCode = "500", description = "Quando ocorre um ERRO INTERNO NO SERVIDOR (When a INTERNAL SERVER ERROR occurs)")
    })
    ResponseEntity<Object> deleteCustomer(Long id) throws CustomerNotFoundException;
}
