package br.com.bancointer.msinvestments.controller.documentation;

import br.com.bancointer.msinvestments.domain.Investment;
import br.com.bancointer.msinvestments.domain.dto.request.InvestmentRequest;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyNotFoundException;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerNotFoundException;
import br.com.bancointer.msinvestments.domain.exception.investment.InsuficientTotalToInvestException;
import br.com.bancointer.msinvestments.domain.exception.investment.InvestmentNotFoundException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

@Tag(name = "Investment")
public interface InvestmentControllerDocumentation {

    @Operation(summary = "Save a investment")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Quando o investimento é CRIADO (When the investment was CREATED)"),
            @ApiResponse(responseCode = "400", description = "Quando o cliente envia um REQUISIÇÃO INCORRETA (When the client sends a BAD REQUEST)"),
            @ApiResponse(responseCode = "404", description = "Quando algum componente do investimento (ex. cliente ou empresa) NÃO FOI ENCONTRADO (When some investment component (i.e. customer or company) was NOT FOUND)"),
            @ApiResponse(responseCode = "500", description = "Quando ocorre um ERRO INTERNO NO SERVIDOR (When a INTERNAL SERVER ERROR occurs)")
    })
    ResponseEntity<Object> saveInvestment(InvestmentRequest investmentRequest) throws InvestmentNotFoundException, CustomerNotFoundException, CompanyNotFoundException, InsuficientTotalToInvestException;

    @Operation(summary = "Get all investments by customer ID and company ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quando a requisição foi OK (When the request was OK)"),
            @ApiResponse(responseCode = "400", description = "Quando o cliente envia um REQUISIÇÃO INCORRETA (When the client sends a BAD REQUEST)"),
            @ApiResponse(responseCode = "404", description = "Quando a investimento NÃO FOI ENCONTRADO (When the investment was NOT FOUND)"),
            @ApiResponse(responseCode = "500", description = "Quando ocorre um ERRO INTERNO NO SERVIDOR (When a INTERNAL SERVER ERROR occurs)")
    })
    ResponseEntity<Page<Investment>> getAllInvestments(@ParameterObject Pageable pageable, Long customerId, Long companyId) throws InvestmentNotFoundException;

    @Operation(summary = "Get one investment")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Quando a requisição foi OK (When the request was OK)"),
            @ApiResponse(responseCode = "404", description = "Quando a investimento NÃO FOI ENCONTRADO (When the investment was NOT FOUND)"),
            @ApiResponse(responseCode = "500", description = "Quando ocorre um ERRO INTERNO NO SERVIDOR (When a INTERNAL SERVER ERROR occurs)")
    })
    ResponseEntity<Object> getOneInvestment(Long id) throws InvestmentNotFoundException;
}
