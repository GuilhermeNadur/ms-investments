package br.com.bancointer.msinvestments.controller.handler;

import br.com.bancointer.msinvestments.domain.dto.ErrorMessageDTO;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyConflictDetectedException;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyNotFoundException;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerConflictDetectedException;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerNotFoundException;
import br.com.bancointer.msinvestments.domain.exception.investment.InsuficientTotalToInvestException;
import br.com.bancointer.msinvestments.domain.exception.investment.InvestmentNotFoundException;
import br.com.bancointer.msinvestments.util.MessageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

@Slf4j
@RestControllerAdvice
public class ControllerExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InsuficientTotalToInvestException.class)
    public ErrorMessageDTO insuficientTotalToInvestExceptionHandler(InsuficientTotalToInvestException exception, WebRequest request) {
        return MessageUtils.getErrorMessageDTO(exception, request, HttpStatus.BAD_REQUEST, LocalDateTime.now());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(InvestmentNotFoundException.class)
    public ErrorMessageDTO investmentNotFoundExceptionHandler(InvestmentNotFoundException exception, WebRequest request) {
        return MessageUtils.getErrorMessageDTO(exception, request, HttpStatus.NOT_FOUND, LocalDateTime.now());
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(CustomerConflictDetectedException.class)
    public ErrorMessageDTO customerConflictDetectedExceptionHandler(CustomerConflictDetectedException exception, WebRequest request) {
        return MessageUtils.getErrorMessageDTO(exception, request, HttpStatus.CONFLICT, LocalDateTime.now());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(CustomerNotFoundException.class)
    public ErrorMessageDTO customerNotFoundExceptionHandler(CustomerNotFoundException exception, WebRequest request) {
        return MessageUtils.getErrorMessageDTO(exception, request, HttpStatus.NOT_FOUND, LocalDateTime.now());
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(CompanyConflictDetectedException.class)
    public ErrorMessageDTO companyConflictDetectedExceptionHandler(CompanyConflictDetectedException exception, WebRequest request) {
        return MessageUtils.getErrorMessageDTO(exception, request, HttpStatus.CONFLICT, LocalDateTime.now());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(CompanyNotFoundException.class)
    public ErrorMessageDTO companyNotFoundRuntimeExceptionHandler(CompanyNotFoundException exception, WebRequest request) {
        return MessageUtils.getErrorMessageDTO(exception, request, HttpStatus.NOT_FOUND, LocalDateTime.now());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorMessageDTO methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException exception, WebRequest request) {
        return MessageUtils.getErrorMessageDTO(exception, request, HttpStatus.BAD_REQUEST, LocalDateTime.now());
    }
}
