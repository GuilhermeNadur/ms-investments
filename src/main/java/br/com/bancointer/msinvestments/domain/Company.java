package br.com.bancointer.msinvestments.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "tb_company")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Company extends RepresentationModel<Company> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    @Column(name = "name")
    private String name;

    @NotEmpty
    @Column(name = "ticker", unique = true)
    private String ticker;

    @NotNull
    @Column(name = "stock_price")
    private BigDecimal stockPrice;

    @NotNull
    @Column(name = "status")
    private Boolean status;

    @JsonIgnore
    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL)
    private List<Investment> investments;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Company company = (Company) o;
        return Objects.equals(id, company.id) && Objects.equals(name, company.name) && Objects.equals(ticker, company.ticker) && Objects.equals(stockPrice, company.stockPrice) && Objects.equals(status, company.status) && Objects.equals(investments, company.investments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, name, ticker, stockPrice, status, investments);
    }
}
