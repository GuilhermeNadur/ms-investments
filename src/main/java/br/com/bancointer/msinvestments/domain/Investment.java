package br.com.bancointer.msinvestments.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "tb_investment")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Investment extends RepresentationModel<Investment> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @JsonIgnore
    @ManyToOne
    private Customer customer;

    @NotNull
    @ManyToOne
    private Company company;

    @NotNull
    @Column(name = "number_shares")
    private Integer numberOfShares;

    @NotNull
    @Column(name = "total")
    private BigDecimal total;

    @Column(name = "change")
    private BigDecimal change;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Investment that = (Investment) o;
        return Objects.equals(id, that.id) && Objects.equals(customer, that.customer) && Objects.equals(company, that.company) && Objects.equals(numberOfShares, that.numberOfShares) && Objects.equals(total, that.total) && Objects.equals(change, that.change);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, customer, company, numberOfShares, total, change);
    }
}
