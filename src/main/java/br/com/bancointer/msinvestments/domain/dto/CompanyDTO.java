package br.com.bancointer.msinvestments.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CompanyDTO {

    @NotEmpty
    @Schema(description = "Nome (Name)", required = true, example = "{\"name\": \"Banco Inter\"}")
    private String name;

    @NotEmpty
    @Schema(description = "Indicador (Ticker)", required = true, example = "{\"ticker\": \"BIDI11\"}")
    private String ticker;

    @NotNull
    @Schema(description = "Preço da Ação (Stock Price)", required = true, example = "{\"stockPrice\": 66.51}")
    private BigDecimal stockPrice;

    @NotNull
    @Schema(description = "Status", required = true, example = "{\"status\": true}")
    private Boolean status;
}
