package br.com.bancointer.msinvestments.domain.dto;

import br.com.bancointer.msinvestments.domain.Investment;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDTO {

    @NotEmpty
    @Schema(description = "Nome (Name)", required = true, example = "{\"name\": \"Banco Inter\"}")
    private String name;

    @CPF
    @NotEmpty
    @Schema(description = "CPF", required = true, example = "{\"cpf\": \"467.589.878-50\"}")
    private String cpf;

    @Hidden
    private List<Investment> investments = new ArrayList<>();
}
