package br.com.bancointer.msinvestments.domain.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InvestmentRequest {

    @CPF
    @NotNull
    @Schema(description = "CPF do Cliente (Customer CPF)", required = true, example = "467.589.878-50")
    private String customerCpf;

    @NotNull
    @Min(value = 1, message = "O total a ser investido deve ser maior que R$ 1,00 (The total to be invested must be greater than R$ 1,00)")
    @Schema(description = "Total a Investir (Total to Invest)", required = true, example = "1946.32")
    private BigDecimal totalToInvest;

    @NotNull
    @Min(value = 1, message = "O número mínimo de empresas a diversificar é 1 (The minimal number of company to diversify is 1)")
    @Schema(description = "Número de Empresas a Diversificar (Number of Companies to Diversify)", required = true, example = "1")
    private Integer numberOfCompaniesToDiversify;
}
