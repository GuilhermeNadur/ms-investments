package br.com.bancointer.msinvestments.domain.dto.response;

import br.com.bancointer.msinvestments.domain.Investment;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class InvestmentResponse {

    @Schema(description = "Investimentos Realizados (Investments Made)")
    private List<Investment> investmentsMade;

    @Schema(description = "Valor Total (Amount)")
    private BigDecimal amount;

    @Schema(description = "Troco (Change)")
    private BigDecimal change;
}
