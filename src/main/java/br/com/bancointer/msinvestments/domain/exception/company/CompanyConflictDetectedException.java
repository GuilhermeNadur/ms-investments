package br.com.bancointer.msinvestments.domain.exception.company;

import java.io.Serial;

public class CompanyConflictDetectedException extends Exception {

    @Serial
    private static final long serialVersionUID = 5731873814901309848L;

    public CompanyConflictDetectedException(Long id, String conflict) {
        super(String.format("The folowing conflict was detected in the company with ID [%d]: %s", id, conflict));
    }
}
