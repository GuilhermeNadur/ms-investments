package br.com.bancointer.msinvestments.domain.exception.company;

import java.io.Serial;

public class CompanyNotFoundException extends Exception {

    @Serial
    private static final long serialVersionUID = 6256378492837192835L;

    public CompanyNotFoundException(String message) {
        super(message);
    }

    public CompanyNotFoundException(Long id) {
        super(String.format("The company with ID [%d] was not found.", id));
    }
}
