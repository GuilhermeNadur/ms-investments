package br.com.bancointer.msinvestments.domain.exception.customer;

import java.io.Serial;

public class CustomerConflictDetectedException extends Exception {

    @Serial
    private static final long serialVersionUID = 9137461799419341439L;

    public CustomerConflictDetectedException(Long id, String conflict) {
        super(String.format("The folowing conflict was detected in the customer with ID [%d]: %s", id, conflict));
    }
}
