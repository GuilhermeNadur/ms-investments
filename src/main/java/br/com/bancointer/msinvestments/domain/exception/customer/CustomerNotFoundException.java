package br.com.bancointer.msinvestments.domain.exception.customer;

import br.com.bancointer.msinvestments.util.MessageUtils;

import java.io.Serial;

public class CustomerNotFoundException extends Exception {

    @Serial
    private static final long serialVersionUID = 1308578103758091334L;

    public CustomerNotFoundException() {
        super(MessageUtils.NO_CUSTOMERS_WAS_FOUND);
    }

    public CustomerNotFoundException(Long id) {
        super(String.format("The customer with ID [%d] was not found.", id));
    }

    public CustomerNotFoundException(String cpf) {
        super(String.format("The customer with CPF [%s] was not found.", cpf));
    }
}
