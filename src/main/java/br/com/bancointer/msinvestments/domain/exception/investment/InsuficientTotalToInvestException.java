package br.com.bancointer.msinvestments.domain.exception.investment;

import java.io.Serial;

public class InsuficientTotalToInvestException extends Exception {

    @Serial
    private static final long serialVersionUID = 7389754897289578923L;

    public InsuficientTotalToInvestException(String message) {
        super(message);
    }
}
