package br.com.bancointer.msinvestments.domain.exception.investment;

import br.com.bancointer.msinvestments.util.MessageUtils;

import java.io.Serial;

public class InvestmentNotFoundException extends Exception {

    @Serial
    private static final long serialVersionUID = 1308578103758091334L;

    public InvestmentNotFoundException() {
        super(MessageUtils.NO_INVESTMENTS_WAS_FOUND);
    }

    public InvestmentNotFoundException(Long id) {
        super(String.format("The investment with ID [%d] was not found.", id));
    }
}
