package br.com.bancointer.msinvestments.mapper;

import br.com.bancointer.msinvestments.domain.Company;
import br.com.bancointer.msinvestments.domain.dto.CompanyDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CompanyMapper {

    CompanyMapper INSTANCE = Mappers.getMapper(CompanyMapper.class);

    Company toModel(CompanyDTO companyDTO);

    CompanyDTO toDTO(Company company);
}
