package br.com.bancointer.msinvestments.mapper;

import br.com.bancointer.msinvestments.domain.Customer;
import br.com.bancointer.msinvestments.domain.dto.CustomerDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    Customer toModel(CustomerDTO customerDTO);

    CustomerDTO toDTO(Customer customer);
}
