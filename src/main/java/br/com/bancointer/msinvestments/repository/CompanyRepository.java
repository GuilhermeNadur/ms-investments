package br.com.bancointer.msinvestments.repository;

import br.com.bancointer.msinvestments.domain.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

    List<Company> findAllByStatus(Boolean status);
    Page<Company> findAllByStatus(Pageable pageable, Boolean status);
    boolean existsByTicker(String ticker);
}
