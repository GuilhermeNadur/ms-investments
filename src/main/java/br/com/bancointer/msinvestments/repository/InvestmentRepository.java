package br.com.bancointer.msinvestments.repository;

import br.com.bancointer.msinvestments.domain.Investment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvestmentRepository extends JpaRepository<Investment, Long> {

    Page<Investment> findAllByCustomerIdAndCompanyId(Pageable pageable, Long customerId, Long companyId);
    Page<Investment> findAllByCustomerId(Pageable pageable, Long customerId);
    Page<Investment> findAllByCompanyId(Pageable pageable, Long companyId);
}
