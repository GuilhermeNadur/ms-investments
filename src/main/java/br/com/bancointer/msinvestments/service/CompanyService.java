package br.com.bancointer.msinvestments.service;

import br.com.bancointer.msinvestments.domain.Company;
import br.com.bancointer.msinvestments.domain.dto.CompanyDTO;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyConflictDetectedException;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.transaction.Transactional;

public interface CompanyService {

    @Transactional
    Company save(CompanyDTO companyDTO) throws CompanyConflictDetectedException, CompanyNotFoundException;

    Page<Company> findAll(Pageable pageable) throws CompanyNotFoundException;

    Page<Company> findAllByStatus(Pageable pageable, Boolean status) throws CompanyNotFoundException;

    Company findById(Long id) throws CompanyNotFoundException;

    @Transactional
    Company update(Long id, CompanyDTO companyDTO) throws CompanyNotFoundException;

    @Transactional
    Company updateToActive(Long id) throws CompanyNotFoundException;

    @Transactional
    Company updateToInactive(Long id) throws CompanyNotFoundException;

    @Transactional
    void deleteById(Long id) throws CompanyNotFoundException;

    boolean existsByTicker(String ticker);
}
