package br.com.bancointer.msinvestments.service;

import br.com.bancointer.msinvestments.domain.Customer;
import br.com.bancointer.msinvestments.domain.dto.CustomerDTO;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerConflictDetectedException;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.transaction.Transactional;

public interface CustomerService {

    @Transactional
    Customer save(CustomerDTO customerDTO) throws CustomerConflictDetectedException, CustomerNotFoundException;

    Page<Customer> findAll(Pageable pageable) throws CustomerNotFoundException;

    Customer findById(Long id) throws CustomerNotFoundException;

    @Transactional
    Customer update(Long id, CustomerDTO customerDTO) throws CustomerNotFoundException;

    @Transactional
    void deleteById(Long id) throws CustomerNotFoundException;
}
