package br.com.bancointer.msinvestments.service;

import br.com.bancointer.msinvestments.domain.Investment;
import br.com.bancointer.msinvestments.domain.dto.request.InvestmentRequest;
import br.com.bancointer.msinvestments.domain.dto.response.InvestmentResponse;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyNotFoundException;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerNotFoundException;
import br.com.bancointer.msinvestments.domain.exception.investment.InsuficientTotalToInvestException;
import br.com.bancointer.msinvestments.domain.exception.investment.InvestmentNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.transaction.Transactional;

public interface InvestmentService {

    @Transactional
    InvestmentResponse save(InvestmentRequest investmentRequest) throws InvestmentNotFoundException, CustomerNotFoundException, CompanyNotFoundException, InsuficientTotalToInvestException;

    Page<Investment> findAllByCustomerIdAndCompanyId(Pageable pageable, Long customerId, Long companyId) throws InvestmentNotFoundException;

    Investment findById(Long id) throws InvestmentNotFoundException;
}
