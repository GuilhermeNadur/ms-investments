package br.com.bancointer.msinvestments.service.impl;

import br.com.bancointer.msinvestments.domain.Company;
import br.com.bancointer.msinvestments.domain.dto.CompanyDTO;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyConflictDetectedException;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyNotFoundException;
import br.com.bancointer.msinvestments.mapper.CompanyMapper;
import br.com.bancointer.msinvestments.repository.CompanyRepository;
import br.com.bancointer.msinvestments.service.CompanyService;
import br.com.bancointer.msinvestments.util.MessageUtils;
import br.com.bancointer.msinvestments.util.hateoas.CompanyHateoasLinkAdder;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class CompanyServiceImpl implements CompanyService {

    private final CompanyRepository companyRepository;
    private final CompanyMapper companyMapper = CompanyMapper.INSTANCE;

    @Transactional
    public Company save(CompanyDTO companyDTO) throws CompanyConflictDetectedException, CompanyNotFoundException {
        var companyToSave = companyMapper.toModel(companyDTO);
        var companySaved = companyRepository.save(throwExceptionIfExistsConflict(companyToSave));
        return CompanyHateoasLinkAdder.addHateoasLinksUsingOneCompany(companySaved);
    }

    public Page<Company> findAll(Pageable pageable) throws CompanyNotFoundException {
        var companyPage = companyRepository.findAll(pageable);
        var companyPageVerified = throwExceptionIfPageIsEmpty(companyPage);
        return CompanyHateoasLinkAdder.addHateoasLinksUsingOneCompany(companyPageVerified);
    }

    public Page<Company> findAllByStatus(Pageable pageable, Boolean status) throws CompanyNotFoundException {
        var companyPage = companyRepository.findAllByStatus(pageable, status);
        var companyPageVerified = throwExceptionIfPageIsEmpty(companyPage);
        return CompanyHateoasLinkAdder.addHateoasLinksUsingOneCompany(companyPageVerified);
    }

    public Company findById(Long id) throws CompanyNotFoundException {
        var companyFound = companyRepository.findById(id).orElseThrow(() -> new CompanyNotFoundException(id));
        return CompanyHateoasLinkAdder.addHateoasLinksUsingAllCompanies(companyFound);
    }

    @Transactional
    public Company update(Long id, CompanyDTO companyDTO) throws CompanyNotFoundException {
        companyRepository.findById(id).orElseThrow(() -> new CompanyNotFoundException(id));
        var companyToUpdate = companyMapper.toModel(companyDTO);
        companyToUpdate.setId(id);
        var companyUpdated = companyRepository.save(companyToUpdate);
        return CompanyHateoasLinkAdder.addHateoasLinksUsingOneCompany(companyUpdated);
    }

    @Transactional
    public Company updateToActive(Long id) throws CompanyNotFoundException {
        var companyToUpdate = companyRepository.findById(id).orElseThrow(() -> new CompanyNotFoundException(id));
        companyToUpdate.setStatus(Boolean.TRUE);
        var companyUpdated = companyRepository.save(companyToUpdate);
        return CompanyHateoasLinkAdder.addHateoasLinksUsingOneCompany(companyUpdated);
    }

    @Transactional
    public Company updateToInactive(Long id) throws CompanyNotFoundException {
        var companyToUpdate = companyRepository.findById(id).orElseThrow(() -> new CompanyNotFoundException(id));
        companyToUpdate.setStatus(Boolean.FALSE);
        var companyUpdated = companyRepository.save(companyToUpdate);
        return CompanyHateoasLinkAdder.addHateoasLinksUsingOneCompany(companyUpdated);
    }

    @Transactional
    public void deleteById(Long id) throws CompanyNotFoundException {
        companyRepository.deleteById(id);
    }

    public boolean existsByTicker(String ticker) {
        return companyRepository.existsByTicker(ticker);
    }

    private Company throwExceptionIfExistsConflict(Company companyToVerify) throws CompanyConflictDetectedException {
        if (existsByTicker(companyToVerify.getTicker())) {
            throw new CompanyConflictDetectedException(companyToVerify.getId(), "ticker already exists");
        }
        return companyToVerify;
    }

    private static Page<Company> throwExceptionIfPageIsEmpty(Page<Company> companyPage) throws CompanyNotFoundException {
        if (companyPage.isEmpty()) {
            throw new CompanyNotFoundException(MessageUtils.NO_COMPANIES_WAS_FOUND);
        }
        return companyPage;
    }
}
