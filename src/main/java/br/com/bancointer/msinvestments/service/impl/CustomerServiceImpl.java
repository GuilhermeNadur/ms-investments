package br.com.bancointer.msinvestments.service.impl;

import br.com.bancointer.msinvestments.domain.Customer;
import br.com.bancointer.msinvestments.domain.dto.CustomerDTO;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerConflictDetectedException;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerNotFoundException;
import br.com.bancointer.msinvestments.mapper.CustomerMapper;
import br.com.bancointer.msinvestments.repository.CustomerRepository;
import br.com.bancointer.msinvestments.service.CustomerService;
import br.com.bancointer.msinvestments.util.hateoas.CustomerHateoasLinkAdder;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final CustomerMapper customerMapper = CustomerMapper.INSTANCE;

    @Transactional
    public Customer save(CustomerDTO customerDTO) throws CustomerConflictDetectedException, CustomerNotFoundException {
        var customerToSave = customerMapper.toModel(customerDTO);
        var customerSaved = customerRepository.save(throwExceptionIfExistsConflict(customerToSave));
        return CustomerHateoasLinkAdder.addHateoasLinksUsingOneCustomer(customerSaved);
    }

    public Page<Customer> findAll(Pageable pageable) throws CustomerNotFoundException {
        var customerPage = customerRepository.findAll(pageable);
        var customerPageVerified = throwExceptionIfPageIsEmpty(customerPage);
        return CustomerHateoasLinkAdder.addHateoasLinksUsingOneCustomer(customerPageVerified);
    }

    public Customer findById(Long id) throws CustomerNotFoundException {
        var customerFound = customerRepository.findById(id).orElseThrow(() -> new CustomerNotFoundException(id));
        return CustomerHateoasLinkAdder.addHateoasLinksUsingAllCustomers(customerFound);
    }

    @Transactional
    public Customer update(Long id, CustomerDTO customerDTO) throws CustomerNotFoundException {
        customerRepository.findById(id).orElseThrow(() -> new CustomerNotFoundException(id));
        var customerToUpdate = customerMapper.toModel(customerDTO);
        customerToUpdate.setId(id);
        var customerUpdated = customerRepository.save(customerToUpdate);
        return CustomerHateoasLinkAdder.addHateoasLinksUsingOneCustomer(customerUpdated);
    }

    @Transactional
    public void deleteById(Long id) throws CustomerNotFoundException {
        customerRepository.deleteById(id);
    }

    private boolean existsByCpf(String cpf) {
        return customerRepository.existsByCpf(cpf);
    }

    private Customer throwExceptionIfExistsConflict(Customer customerToVerify) throws CustomerConflictDetectedException {
        if (existsByCpf(customerToVerify.getCpf())) {
            throw new CustomerConflictDetectedException(customerToVerify.getId(), "cpf already exists");
        }
        return customerToVerify;
    }

    private static Page<Customer> throwExceptionIfPageIsEmpty(Page<Customer> customerPage) throws CustomerNotFoundException {
        if (customerPage.isEmpty()) {
            throw new CustomerNotFoundException();
        }
        return customerPage;
    }
}
