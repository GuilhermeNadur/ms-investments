package br.com.bancointer.msinvestments.service.impl;

import br.com.bancointer.msinvestments.domain.Company;
import br.com.bancointer.msinvestments.domain.Customer;
import br.com.bancointer.msinvestments.domain.Investment;
import br.com.bancointer.msinvestments.domain.dto.request.InvestmentRequest;
import br.com.bancointer.msinvestments.domain.dto.response.InvestmentResponse;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyNotFoundException;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerNotFoundException;
import br.com.bancointer.msinvestments.domain.exception.investment.InsuficientTotalToInvestException;
import br.com.bancointer.msinvestments.domain.exception.investment.InvestmentNotFoundException;
import br.com.bancointer.msinvestments.repository.CompanyRepository;
import br.com.bancointer.msinvestments.repository.CustomerRepository;
import br.com.bancointer.msinvestments.repository.InvestmentRepository;
import br.com.bancointer.msinvestments.service.InvestmentService;
import br.com.bancointer.msinvestments.util.DiversifierUtils;
import br.com.bancointer.msinvestments.util.ListUtils;
import br.com.bancointer.msinvestments.util.MessageUtils;
import br.com.bancointer.msinvestments.util.hateoas.InvestmentsHateoasLinkAdder;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.*;

@Service
@AllArgsConstructor
public class InvestmentServiceImpl implements InvestmentService {

    private final InvestmentRepository investmentRepository;
    private final CustomerRepository customerRepository;
    private final CompanyRepository companyRepository;

    @Transactional
    public InvestmentResponse save(InvestmentRequest investmentRequest) throws InvestmentNotFoundException, CustomerNotFoundException, CompanyNotFoundException, InsuficientTotalToInvestException {
        var customer = getCustomerByCpfFromRepository(customerRepository, investmentRequest.getCustomerCpf());
        var activeCompanies = getActiveCompaniesFromRepository(companyRepository);
        var investmentsToSave = getDiversifiedInvestments(investmentRequest, activeCompanies, customer);
        var investmentsSaved = investmentRepository.saveAll(investmentsToSave);
        var investmentsSavedWithHateoasLinks = InvestmentsHateoasLinkAdder.addHateoasLinksUsingOneInvestment(investmentsSaved);
        return convertToResponse(investmentsSavedWithHateoasLinks);
    }

    public Page<Investment> findAllByCustomerIdAndCompanyId(Pageable pageable, Long customerId, Long companyId) throws InvestmentNotFoundException {
        var investmentList = getInvestmentListFromRepository(investmentRepository, pageable, customerId, companyId);
        var investmentListVerified = throwExceptionIfPageIsEmpty(investmentList);
        return InvestmentsHateoasLinkAdder.addHateoasLinksUsingOneInvestment(investmentListVerified);
    }

    public Investment findById(Long id) throws InvestmentNotFoundException {
        var investmentFound = investmentRepository.findById(id).orElseThrow(() -> new InvestmentNotFoundException(id));
        return InvestmentsHateoasLinkAdder.addHateoasLinksUsingAllInvestments(investmentFound);
    }

    private static List<Company> getActiveCompaniesFromRepository(CompanyRepository companyRepository) throws CompanyNotFoundException {
        var activeCompanies = companyRepository.findAllByStatus(Boolean.TRUE);
        throwExceptionIfThereAreNoActiveCompanyToInvest(activeCompanies);
        return activeCompanies;
    }

    private static Customer getCustomerByCpfFromRepository(CustomerRepository customerRepository, String customerCpf) throws CustomerNotFoundException {
        return customerRepository.findByCpf(customerCpf).orElseThrow(() -> new CustomerNotFoundException(customerCpf));
    }

    private static List<Investment> getDiversifiedInvestments(InvestmentRequest investmentRequest, List<Company> activeCompanies, Customer customer) throws InsuficientTotalToInvestException {
        var numberOfCompaniesToDiversify = investmentRequest.getNumberOfCompaniesToDiversify();
        var totalToInvest = investmentRequest.getTotalToInvest();
        var possibleCompaniesToInvest = filterAndSortCompanies(activeCompanies, totalToInvest);
        throwExceptionIfThereAreNoPossibleCompaniesToInvest(possibleCompaniesToInvest);
        return doGetDiversifiedInvestments(totalToInvest, numberOfCompaniesToDiversify, possibleCompaniesToInvest, customer);
    }

    private static void throwExceptionIfThereAreNoActiveCompanyToInvest(List<Company> companies) throws CompanyNotFoundException {
        if (CollectionUtils.isEmpty(companies)) {
            throw new CompanyNotFoundException(MessageUtils.AT_LEAST_ONE_COMPANY_MUST_BE_ACTIVE_TO_INVEST);
        }
    }

    private static void throwExceptionIfThereAreNoPossibleCompaniesToInvest(List<Company> companies) throws InsuficientTotalToInvestException {
        if (CollectionUtils.isEmpty(companies)) {
            throw new InsuficientTotalToInvestException(MessageUtils.TOTAL_TO_INVEST_MUST_BE_HIGHER_THAN_CHEAPEST_STOCK);
        }
    }

    private static List<Investment> doGetDiversifiedInvestments(BigDecimal totalToInvest, Integer numberOfCompaniesToDiversify, List<Company> possibleCompaniesToInvest, Customer customer) {
        var investmentsMade = new ArrayList<Investment>();
        var chosenCompanies = getChosenCompanies(possibleCompaniesToInvest, numberOfCompaniesToDiversify, customer);
        var lowestShareValueOfChosenCompanies = getCompanyWithlowestShareValue(chosenCompanies).getStockPrice();
        var companiesToInvestCycleIterator = ListUtils.getCycleStream(chosenCompanies).iterator();
        while (totalToInvest.compareTo(lowestShareValueOfChosenCompanies) >= 0) {
            var companyToInvest = getNextCompanyToInvest(companiesToInvestCycleIterator, totalToInvest);
            createNewInvestmentAndAddToListOrUpdateExistingInvestment(companyToInvest, investmentsMade, customer);
            totalToInvest = totalToInvest.subtract(companyToInvest.getStockPrice());
        }
        ListUtils.getLastElement(investmentsMade).setChange(totalToInvest);
        return investmentsMade;
    }

    private static List<Company> getChosenCompanies(List<Company> possibleCompaniesToInvest, Integer numberOfCompaniesToDiversify, Customer customer) {
        var chosenCompanies = new ArrayList<Company>();
        var companiesHasNeverInvested = getCompaniesWhoseClientHasNeverInvestedFromList(customer, possibleCompaniesToInvest);
        var companiesAlreadyInvested = ListUtils.removeAllSelectedObjectsFromList(possibleCompaniesToInvest, companiesHasNeverInvested);
        Iterator<String> diversifier;
        boolean diversifierStartedWithSemiRandom;
        if (companiesHasNeverInvested.isEmpty()) {
            diversifier = DiversifierUtils.getSemiRandomDiversifier();
            diversifierStartedWithSemiRandom = true;
        } else {
            diversifier = DiversifierUtils.getDefaultDiversifier();
            diversifierStartedWithSemiRandom = false;
        }
        while (numberOfCompaniesToDiversify > 0 && !ListUtils.isEmpty(companiesHasNeverInvested, companiesAlreadyInvested)) {
            var nextChosenCompany = getNextChosenCompany(companiesHasNeverInvested, companiesAlreadyInvested, diversifier);
            chosenCompanies.add(nextChosenCompany);
            numberOfCompaniesToDiversify--;
            if (companiesHasNeverInvested.isEmpty() && !diversifierStartedWithSemiRandom) {
                diversifier = DiversifierUtils.getSemiRandomDiversifier();
                diversifierStartedWithSemiRandom = true;
            }
        }
        return chosenCompanies;
    }

    private static List<Company> getCompaniesWhoseClientHasNeverInvestedFromList(Customer customer, List<Company> possibleCompaniesToInvest) {
        var companiesAlreadyInvested = getCompaniesAlreadyInvestedByCustomer(customer);
        return ListUtils.removeAllSelectedObjectsFromList(possibleCompaniesToInvest, companiesAlreadyInvested);
    }

    private static List<Company> getCompaniesAlreadyInvestedByCustomer(Customer customer) {
        var companiesAlreadyInvested = new ArrayList<Company>();
        for (var investments : customer.getInvestments()) {
            if (!companiesAlreadyInvested.contains(investments.getCompany())) {
                companiesAlreadyInvested.add(investments.getCompany());
            }
        }
        return companiesAlreadyInvested;
    }

    private static Company getNextChosenCompany(List<Company> companiesHasNeverInvested, List<Company> companiesAlreadyInvested, Iterator<String> diversifier) {
        if (companiesHasNeverInvested.isEmpty()) {
            return getNextChosenCompanyFromListOfCompaniesAlreadyInvested(companiesAlreadyInvested, diversifier);
        }
        return getNextChosenCompanyFromListOfCompaniesHasNeverInvested(companiesHasNeverInvested, diversifier);
    }

    private static Company getNextChosenCompanyFromListOfCompaniesAlreadyInvested(List<Company> companiesAlreadyInvested, Iterator<String> diversifier) {
        var nextPosition = diversifier.next();
        var nextCompany = ListUtils.getElementByPosition(companiesAlreadyInvested, nextPosition);
        companiesAlreadyInvested.remove(nextCompany);
        return nextCompany;
    }

    private static Company getNextChosenCompanyFromListOfCompaniesHasNeverInvested(List<Company> companiesHasNeverInvested, Iterator<String> diversifier) {
        var nextPosition = diversifier.next();
        var nextCompany = ListUtils.getElementByPosition(companiesHasNeverInvested, nextPosition);
        companiesHasNeverInvested.remove(nextCompany);
        return nextCompany;
    }

    private static Company getCompanyWithlowestShareValue(List<Company> chosenCompanies) {
        return chosenCompanies.stream().min(Comparator.comparing(Company::getStockPrice)).orElseThrow();
    }

    private static Company getNextCompanyToInvest(Iterator<Company> companiesToInvestCycleIterator, BigDecimal totalToInvest) {
        var nextCompany = companiesToInvestCycleIterator.next();
        if (nextCompany.getStockPrice().compareTo(totalToInvest) <= 0) {
            return nextCompany;
        }
        return getNextCompanyToInvest(companiesToInvestCycleIterator, totalToInvest);
    }

    private static void createNewInvestmentAndAddToListOrUpdateExistingInvestment(Company companyToInvest, List<Investment> investmentsMade, Customer customer) {
        if (isAlreadySomeInvestmentMadeInThisCompany(investmentsMade, companyToInvest)) {
            updateInvestmentAddingOneMoreShare(companyToInvest, investmentsMade);
        } else {
            addNewInvestment(companyToInvest, investmentsMade, customer);
        }
    }

    private static void updateInvestmentAddingOneMoreShare(Company companyToInvest, List<Investment> investmentsMade) {
        investmentsMade.stream()
                .filter(investment -> Objects.equals(investment.getCompany().getId(), companyToInvest.getId()))
                .forEach(investment -> {
                    investment.setNumberOfShares(investment.getNumberOfShares() + 1);
                    investment.setTotal(investment.getTotal().add(companyToInvest.getStockPrice()));
                });
    }

    private static void addNewInvestment(Company companyToInvest, List<Investment> investmentsMade, Customer customer) {
        investmentsMade.add(Investment.builder()
                .customer(customer)
                .company(companyToInvest)
                .numberOfShares(1)
                .total(companyToInvest.getStockPrice())
                .change(BigDecimal.ZERO)
                .build());
    }

    private static boolean isAlreadySomeInvestmentMadeInThisCompany(List<Investment> investmentsMade, Company companyToVerify) {
        return investmentsMade.stream().anyMatch(investment -> Objects.equals(investment.getCompany().getId(), companyToVerify.getId()));
    }

    private static List<Company> filterAndSortCompanies(List<Company> companies, BigDecimal totalToInvest) {
        return companies.stream()
                .filter(company -> company.getStockPrice().compareTo(totalToInvest) <= 0)
                .sorted(Comparator.comparing(Company::getStockPrice).reversed())
                .toList();
    }

    private static InvestmentResponse convertToResponse(List<Investment> investmentsToReponse) {
        var amount = getAmountFromInvestmentList(investmentsToReponse);
        var change = getChangeFromInvestmentList(investmentsToReponse);
        return InvestmentResponse.builder()
                .investmentsMade(investmentsToReponse)
                .amount(amount)
                .change(change)
                .build();
    }

    private static BigDecimal getAmountFromInvestmentList(List<Investment> investments) {
        var amount = BigDecimal.ZERO;
        for (var investment : investments) {
            amount = amount.add(investment.getTotal());
        }
        return amount;
    }

    private static BigDecimal getChangeFromInvestmentList(List<Investment> investments) {
        var change = BigDecimal.ZERO;
        for (var investment : investments) {
            change = change.add(investment.getChange());
        }
        return change;
    }

    private static Page<Investment> throwExceptionIfPageIsEmpty(Page<Investment> investmentPage) throws InvestmentNotFoundException {
        if (investmentPage.isEmpty()) {
            throw new InvestmentNotFoundException();
        }
        return investmentPage;
    }

    private static Page<Investment> getInvestmentListFromRepository(InvestmentRepository investmentRepository, Pageable pageable, Long customerId, Long companyId) {
        if (customerId != null && companyId != null) {
            return investmentRepository.findAllByCustomerIdAndCompanyId(pageable, customerId, companyId);
        }
        if (customerId != null) {
            return investmentRepository.findAllByCustomerId(pageable, customerId);
        }
        if (companyId != null) {
            return investmentRepository.findAllByCompanyId(pageable, companyId);
        }
        return investmentRepository.findAll(pageable);
    }
}
