package br.com.bancointer.msinvestments.util;

import java.util.Arrays;
import java.util.Iterator;

public class DiversifierUtils {

    public static final String LAST = "Last";
    public static final String MIDDLE = "Middle";
    public static final String FIRST = "First";
    public static final String RANDOM = "Random";

    private DiversifierUtils() {}

    public static Iterator<String> getDefaultDiversifier() {
        var listToStream = Arrays.asList(LAST, MIDDLE, FIRST);
        return ListUtils.getCycleStream(listToStream).iterator();
    }

    public static Iterator<String> getSemiRandomDiversifier() {
        var listToStream = Arrays.asList(RANDOM, LAST, RANDOM, MIDDLE, RANDOM, FIRST);
        return ListUtils.getCycleStream(listToStream).iterator();
    }
}
