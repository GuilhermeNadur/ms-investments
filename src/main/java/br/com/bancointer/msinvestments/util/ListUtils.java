package br.com.bancointer.msinvestments.util;

import java.util.*;
import java.util.stream.Stream;

public class ListUtils {

    private static final Random RANDOM = new Random();

    private ListUtils() {}

    public static <T> Stream<T> getCycleStream(List<T> supplier) {
        return Stream.generate(() -> supplier).flatMap(Collection::stream);
    }

    @SafeVarargs
    public static <T> boolean isEmpty(List<T>... lists) {
        var isEmpty = true;
        if (lists != null) {
            for (var list : lists) {
                if (!list.isEmpty()) {
                    isEmpty = false;
                    break;
                }
            }
        }
        return isEmpty;
    }

    public static <T> T getFirstElement(List<T> list) {
        return list != null && !list.isEmpty() ? list.get(0) : null;
    }

    public static <T> T getMiddleElement(List<T> list) {
        if (list != null && !list.isEmpty()) {
            if (list.size() == 1) {
                return list.get(0);
            }
            return list.get((list.size() / 2));
        }
        return null;
    }

    public static <T> T getLastElement(List<T> list) {
        return list != null && !list.isEmpty() ? list.get(list.size() - 1) : null;
    }

    public static <T> T getRandomElement(List<T> list) {
        return list != null && !list.isEmpty()
                ? list.get(RANDOM.nextInt(list.size()))
                : null;
    }

    public static <T> T getElementByPosition(List<T> list, String position) {
        Objects.requireNonNull(list);
        if (position.equals(DiversifierUtils.FIRST)) {
            return getFirstElement(list);
        }
        if (position.equals(DiversifierUtils.MIDDLE)) {
            return getMiddleElement(list);
        }
        if (position.equals(DiversifierUtils.LAST)) {
            return getLastElement(list);
        }
        if (position.equals(DiversifierUtils.RANDOM)) {
            return getRandomElement(list);
        }
        return null;
    }

    public static <T> List<T> removeAllSelectedObjectsFromList(List<T> list, List<T> objectsToRemove) {
        var newList = new ArrayList<>(list);
        if (objectsToRemove != null) {
            newList.removeAll(objectsToRemove);
        }
        return newList;
    }
}
