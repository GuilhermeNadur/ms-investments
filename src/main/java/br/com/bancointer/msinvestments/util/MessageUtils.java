package br.com.bancointer.msinvestments.util;

import br.com.bancointer.msinvestments.domain.dto.ErrorMessageDTO;
import br.com.bancointer.msinvestments.domain.dto.SuccessMessageDTO;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

public class MessageUtils {

    private MessageUtils() {}

    public static final String NO_COMPANIES_WAS_FOUND = "Nenhuma empresa foi encontrada (No companies was found)";
    public static final String NO_INVESTMENTS_WAS_FOUND = "Nenhum investimento foi encontrado (No investments was found)";
    public static final String NO_CUSTOMERS_WAS_FOUND = "Nenhum cliente foi encontrado (No customers was found)";

    public static final String AT_LEAST_ONE_COMPANY_MUST_BE_ACTIVE_TO_INVEST = "Ao menos uma empresa deve estar ativa para investir (At least one company must be active to invest)";
    public static final String TOTAL_TO_INVEST_MUST_BE_HIGHER_THAN_CHEAPEST_STOCK = "O valor a investir deve ser superior a ação mais barata (The total to invest must be higher than the cheapest stock)";

    public static ErrorMessageDTO getErrorMessageDTO(Exception exception, WebRequest request, HttpStatus httpStatus, LocalDateTime localDateTime) {
        return ErrorMessageDTO.builder()
                .statusCode(httpStatus.value())
                .localDateTime(localDateTime)
                .message(exception.getMessage())
                .stackTrace(ExceptionUtils.getStackTrace(exception))
                .description(request.getDescription(false))
                .build();
    }

    public static SuccessMessageDTO getDeleteSuccessMessageDTO(String domainName, Long id) {
        return SuccessMessageDTO.builder()
                .statusCode(HttpStatus.OK.value())
                .localDateTime(LocalDateTime.now())
                .message(String.format("A entidade [%1$s] com ID [%2$d] foi deletado com sucesso (The entity [%1$s] with ID [%2$d] was successfully deleted)", domainName, id))
                .build();
    }
}
