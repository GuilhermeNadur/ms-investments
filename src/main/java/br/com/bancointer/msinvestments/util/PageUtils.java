package br.com.bancointer.msinvestments.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class PageUtils {

    private PageUtils() {}

    public static Pageable getDefaultPageRequest() {
        return PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
    }
}
