package br.com.bancointer.msinvestments.util.hateoas;

import br.com.bancointer.msinvestments.controller.CompanyController;
import br.com.bancointer.msinvestments.domain.Company;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyNotFoundException;
import br.com.bancointer.msinvestments.util.PageUtils;
import org.springframework.data.domain.Page;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public class CompanyHateoasLinkAdder {

    private CompanyHateoasLinkAdder() {}

    public static Company addHateoasLinksUsingAllCompanies(Company companyToAddLink) throws CompanyNotFoundException {
        return companyToAddLink.add(linkTo(methodOn(CompanyController.class).getAllCompanies(PageUtils.getDefaultPageRequest())).withRel("company_list"));
    }

    public static Company addHateoasLinksUsingOneCompany(Company companyToAddLink) throws CompanyNotFoundException {
        return companyToAddLink.add(linkTo(methodOn(CompanyController.class).getOneCompany(companyToAddLink.getId())).withSelfRel());
    }

    public static Page<Company> addHateoasLinksUsingOneCompany(Page<Company> companyPageToAddLinks) throws CompanyNotFoundException {
        for (var company : companyPageToAddLinks) {
            addHateoasLinksUsingOneCompany(company);
        }
        return companyPageToAddLinks;
    }
}
