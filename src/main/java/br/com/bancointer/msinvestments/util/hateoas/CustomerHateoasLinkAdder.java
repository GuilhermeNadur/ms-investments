package br.com.bancointer.msinvestments.util.hateoas;

import br.com.bancointer.msinvestments.controller.CustomerController;
import br.com.bancointer.msinvestments.domain.Customer;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerNotFoundException;
import br.com.bancointer.msinvestments.util.PageUtils;
import org.springframework.data.domain.Page;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public class CustomerHateoasLinkAdder {

    private CustomerHateoasLinkAdder() {}

    public static Customer addHateoasLinksUsingAllCustomers(Customer customerToAddLink) throws CustomerNotFoundException {
        return customerToAddLink.add(linkTo(methodOn(CustomerController.class).getAllCustomers(PageUtils.getDefaultPageRequest())).withRel("customer_list"));
    }

    public static Customer addHateoasLinksUsingOneCustomer(Customer customerToAddLink) throws CustomerNotFoundException {
        return customerToAddLink.add(linkTo(methodOn(CustomerController.class).getOneCustomer(customerToAddLink.getId())).withSelfRel());
    }

    public static Page<Customer> addHateoasLinksUsingOneCustomer(Page<Customer> customerPageToAddLinks) throws CustomerNotFoundException {
        for (var customer : customerPageToAddLinks) {
            addHateoasLinksUsingOneCustomer(customer);
        }
        return customerPageToAddLinks;
    }
}
