package br.com.bancointer.msinvestments.util.hateoas;

import br.com.bancointer.msinvestments.controller.InvestmentController;
import br.com.bancointer.msinvestments.domain.Investment;
import br.com.bancointer.msinvestments.domain.exception.investment.InvestmentNotFoundException;
import br.com.bancointer.msinvestments.util.PageUtils;
import org.springframework.data.domain.Page;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public class InvestmentsHateoasLinkAdder {

    private InvestmentsHateoasLinkAdder() {}

    public static Investment addHateoasLinksUsingAllInvestments(Investment investmentToAddLink) throws InvestmentNotFoundException {
        var customerId = investmentToAddLink.getCustomer().getId();
        var companyId = investmentToAddLink.getCompany().getId();
        return investmentToAddLink.add(linkTo(methodOn(InvestmentController.class).getAllInvestments(PageUtils.getDefaultPageRequest(), customerId, companyId)).withRel("investment_list"));
    }

    public static void addHateoasLinksUsingOneInvestment(Investment investmentToAddLink) throws InvestmentNotFoundException {
        investmentToAddLink.add(linkTo(methodOn(InvestmentController.class).getOneInvestment(investmentToAddLink.getId())).withSelfRel());
    }

    public static Page<Investment> addHateoasLinksUsingOneInvestment(Page<Investment> investmentPageToAddLinks) throws InvestmentNotFoundException {
        for (var investment : investmentPageToAddLinks) {
            addHateoasLinksUsingOneInvestment(investment);
        }
        return investmentPageToAddLinks;
    }

    public static List<Investment> addHateoasLinksUsingOneInvestment(List<Investment> investmentPageToAddLinks) throws InvestmentNotFoundException {
        for (var investment : investmentPageToAddLinks) {
            addHateoasLinksUsingOneInvestment(investment);
        }
        return investmentPageToAddLinks;
    }
}
