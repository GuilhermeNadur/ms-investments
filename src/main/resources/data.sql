INSERT INTO tb_company (name, ticker, stock_price, status)
VALUES ('Banco Inter', 'BIDI11', 66.51, true),
    ('Magazine Luiza', 'MGLU3', 18.80, true),
    ('Sul América', 'SULA11', 28.26, true),
    ('Engie', 'EGIE3', 38.30, true),
    ('CVC Brasil', 'CVCB3', 20.87, true),
    ('Lojas Renner', 'LREN3', 36.95, true),
    ('Marisa', 'AMAR3', 6.30, true);

INSERT INTO tb_customer (name, cpf)
VALUES ('Guilherme Rodrigues Nadur', '467.589.878-50');