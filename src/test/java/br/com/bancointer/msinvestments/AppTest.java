package br.com.bancointer.msinvestments;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = { App.class })
public class AppTest {

    // Test class added ONLY to cover main() invocation not covered by application tests
    @Test
    public void main() {
        App.main(new String[] {});
    }
}
