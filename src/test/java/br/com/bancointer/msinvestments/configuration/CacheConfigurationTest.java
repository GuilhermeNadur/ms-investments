package br.com.bancointer.msinvestments.configuration;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.CacheManager;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = { CacheConfiguration.class })
public class CacheConfigurationTest {

    @Autowired
    private CacheManager cacheManager;

    @Value("${application.cache.names}")
    private String[] applicationCacheNames;

    @Test
    @DisplayName("Quando o Spring Boot é inicializado, ele deve configurar o Bean Cache Manager. When Spring Boot boots, it should set the Bean Cache Manager.")
    void cacheManager() {
        assertTrue(cacheManager.getCacheNames().contains(applicationCacheNames[0]));
        assertTrue(cacheManager.getCacheNames().contains(applicationCacheNames[1]));
        assertTrue(cacheManager.getCacheNames().contains(applicationCacheNames[2]));
        assertTrue(cacheManager.getCacheNames().contains(applicationCacheNames[3]));
        assertTrue(cacheManager.getCacheNames().contains(applicationCacheNames[4]));
        assertTrue(cacheManager.getCacheNames().contains(applicationCacheNames[5]));
        assertTrue(cacheManager.getCacheNames().contains(applicationCacheNames[6]));
    }
}