package br.com.bancointer.msinvestments.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = { DateConfiguration.class })
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class DateConfigurationTest {

    private final ObjectMapper objectMapper;

    @Test
    @DisplayName("Quando o Spring Boot é inicializado, ele deve configurar o Bean ObjectMapper. When Spring Boot boots, it should set the Bean ObjectMapper.")
    void cacheManager() {
        assertNotNull(objectMapper);
    }
}