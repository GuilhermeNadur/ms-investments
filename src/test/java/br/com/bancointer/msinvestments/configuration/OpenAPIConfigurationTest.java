package br.com.bancointer.msinvestments.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import lombok.AllArgsConstructor;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@SpringBootTest(classes = { OpenAPIConfiguration.class })
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class OpenAPIConfigurationTest {

    private final OpenAPI microserviceInvestmentsOpenAPI;

    @Test
    @DisplayName("Quando o Spring Boot é inicializado, ele deve configurar o Bean OpenAPI. When Spring Boot boots, it should set the Bean OpenAPI.")
    void cacheManager() {
        assertThat("Microservice Investments - RESTful API", is(equalTo(microserviceInvestmentsOpenAPI.getInfo().getTitle())));
    }
}