package br.com.bancointer.msinvestments.controller;

import br.com.bancointer.msinvestments.domain.dto.CompanyDTO;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyConflictDetectedException;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyNotFoundException;
import br.com.bancointer.msinvestments.mapper.CompanyMapper;
import br.com.bancointer.msinvestments.service.CompanyService;
import br.com.bancointer.msinvestments.test.TestConstants;
import br.com.bancointer.msinvestments.test.TestUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(CompanyController.class)
class CompanyControllerTest {

    private final CompanyMapper companyMapper = CompanyMapper.INSTANCE;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CompanyService companyService;

    @InjectMocks
    private CompanyController companyController;

    // Section: SAVE
    @Test
    @DisplayName("Quando o método saveCompany() é chamado com um CompanyDTO válido, então deve retornar o status code 200 com a empresa criada no body." +
            "When the method saveCompany() is called with a valid CompanyDTO, then it should be return status code 200 with the company created in the body.")
    void saveCompany_201() throws Exception {
        var company = TestUtils.getSampleCompanyWithoutId();
        var companyDTO = companyMapper.toDTO(company);
        when(companyService.save(any(CompanyDTO.class))).thenReturn(company);
        mockMvc.perform(post(TestConstants.API_V1_COMPANY)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtils.convertToJSON(companyDTO)))
                        .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(companyDTO.getName())))
                .andExpect(jsonPath("$.ticker", is(companyDTO.getTicker())))
                .andExpect(jsonPath("$.stockPrice", is(companyDTO.getStockPrice().intValue())))
                .andExpect(jsonPath("$.status", is(companyDTO.getStatus())));
    }

    @Test
    @DisplayName("Quando o método saveCompany() é chamado com um CompanyDTO inválido, então deve retornar o status code 400." +
            "When the method saveCompany() is called with a invalid CompanyDTO, then it should be return status code 400.")
    void saveCompany_400() throws Exception {
        var company = TestUtils.getEmptyCompany();
        var companyDTO = companyMapper.toDTO(company);
        mockMvc.perform(post(TestConstants.API_V1_COMPANY)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtils.convertToJSON(companyDTO)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Quando o método saveCompany() é chamado com um CompanyDTO em conflito, então deve retornar o status code 409." +
            "When the method saveCompany() is called with a conflicted CompanyDTO, then it should be return status code 409.")
    void saveCompany_409() throws Exception {
        var company = TestUtils.getSampleCompanyWithId(1L);
        var companyDTO = companyMapper.toDTO(company);
        when(companyService.save(any(CompanyDTO.class))).thenThrow(CompanyConflictDetectedException.class);
        mockMvc.perform(post(TestConstants.API_V1_COMPANY)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtils.convertToJSON(companyDTO)))
                .andExpect(status().isConflict());
    }

    // Section: GET ALL
    @Test
    @DisplayName("Quando o método getAllCompanies() é chamado com uma propriedade válida (ex. Pageable), então deve retornar o status code 200 com a lista de empresas encontradas no body." +
            "When the method getAllCompanies() is called with a valid property (i.e. Pageable), then it should be return status code 200 with the list of companies found in the body.")
    void getAllCompanies_200() throws Exception {
        var companyToCompare = TestUtils.getSampleCompanyWithId(1L);
        var companyList = TestUtils.getSamplePageCompany();
        when(companyService.findAll(any(Pageable.class))).thenReturn(companyList);
        mockMvc.perform(get(TestConstants.API_V1_COMPANY)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].id", is(companyToCompare.getId().intValue())))
                .andExpect(jsonPath("$.content[0].name", is(companyToCompare.getName())))
                .andExpect(jsonPath("$.content[0].ticker", is(companyToCompare.getTicker())))
                .andExpect(jsonPath("$.content[0].stockPrice", is(companyToCompare.getStockPrice().intValue())))
                .andExpect(jsonPath("$.content[0].status", is(companyToCompare.getStatus())));
    }

    @Test
    @DisplayName("Quando o método getAllCompanies() é chamado, mas não existe nenhuma empresa no banco de dados, então deve retornar o status code 404." +
            "When the method getAllCompanies() is called, but there are no companies in the database, then it should be return status code 404.")
    void getAllCompanies_404() throws Exception {
        when(companyService.findAll(any(Pageable.class))).thenThrow(CompanyNotFoundException.class);
        mockMvc.perform(get(TestConstants.API_V1_COMPANY)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Quando o método getAllActiveCompanies() é chamado com uma propriedade válida (ex. Pageable), então deve retornar o status code 200 com a lista de empresas ativas encontradas no body." +
            "When the method getAllActiveCompanies() is called with a valid property (i.e. Pageable), then it should be return status code 200 with the list of active companies found in the body.")
    void getAllActiveCompanies_200() throws Exception {
        var companyToCompare = TestUtils.getSampleCompanyWithId(1L);
        var companyList = TestUtils.getSamplePageCompany();
        when(companyService.findAllByStatus(any(Pageable.class), any(Boolean.class))).thenReturn(companyList);
        mockMvc.perform(get(TestConstants.API_V1_COMPANY_ACTIVE)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].id", is(companyToCompare.getId().intValue())))
                .andExpect(jsonPath("$.content[0].name", is(companyToCompare.getName())))
                .andExpect(jsonPath("$.content[0].ticker", is(companyToCompare.getTicker())))
                .andExpect(jsonPath("$.content[0].stockPrice", is(companyToCompare.getStockPrice().intValue())))
                .andExpect(jsonPath("$.content[0].status", is(companyToCompare.getStatus())));
    }

    // Section: GET ONE
    @Test
    @DisplayName("Quando o método getOneCompany() é chamado com um ID válido, então deve retornar o status code 200 com a empresa encontrada no body." +
            "When the method getOneCompany() is called with a valid ID, then it should be return status code 200 with the company found in the body.")
    void getOneCompany_200() throws Exception {
        var companyToCompare = TestUtils.getSampleCompanyWithId(1L);
        when(companyService.findById(any(Long.class))).thenReturn(companyToCompare);
        mockMvc.perform(get(TestConstants.API_V1_COMPANY_1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(companyToCompare.getId().intValue())))
                .andExpect(jsonPath("$.name", is(companyToCompare.getName())))
                .andExpect(jsonPath("$.ticker", is(companyToCompare.getTicker())))
                .andExpect(jsonPath("$.stockPrice", is(companyToCompare.getStockPrice().intValue())))
                .andExpect(jsonPath("$.status", is(companyToCompare.getStatus())));
    }

    @Test
    @DisplayName("Quando o método getOneCompany() é chamado com um ID inválido, então deve retornar o status code 404." +
            "When the method getOneCompany() is called with a invalid ID, then it should be return status code 404.")
    void getOneCompany_404() throws Exception {
        when(companyService.findById(any(Long.class))).thenThrow(CompanyNotFoundException.class);
        mockMvc.perform(get(TestConstants.API_V1_COMPANY_1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    // Section: UPDATE
    @Test
    @DisplayName("Quando o método updateCompany() é chamado com um ID válido, então deve retornar o status code 200 com a empresa atualizada no body." +
            "When the method updateCompany() is called with a valid ID and CompanyDTO, then it should be return status code 200 with the company updated in the body.")
    void updateCompany_200() throws Exception {
        var company = TestUtils.getSampleCompanyWithId(1L);
        var companyDTO = companyMapper.toDTO(company);
        when(companyService.update(any(Long.class), any(CompanyDTO.class))).thenReturn(company);
        mockMvc.perform(put(TestConstants.API_V1_COMPANY_1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtils.convertToJSON(companyDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(company.getId().intValue())))
                .andExpect(jsonPath("$.name", is(company.getName())))
                .andExpect(jsonPath("$.ticker", is(company.getTicker())))
                .andExpect(jsonPath("$.stockPrice", is(company.getStockPrice().intValue())))
                .andExpect(jsonPath("$.status", is(company.getStatus())));
    }

    @Test
    @DisplayName("Quando o método updateCompany() é chamado com um ID e CompanyDTO válido, então deve retornar o status code 404." +
            "When the method updateCompany() is called with a invalid ID or CompanyDTO, then it should be return status code 404.")
    void updateCompany_404() throws Exception {
        var company = TestUtils.getSampleCompanyWithId(1L);
        var companyDTO = companyMapper.toDTO(company);
        when(companyService.update(any(Long.class), any(CompanyDTO.class))).thenThrow(CompanyNotFoundException.class);
        mockMvc.perform(put(TestConstants.API_V1_COMPANY_1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtils.convertToJSON(companyDTO)))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Quando o método updateCompanyToActive() é chamado com um ID válido, então deve retornar o status code 200 com a empresa atualizada no body." +
            "When the method updateCompanyToActive() is called with a valid ID, then it should be return status code 200 with the company updated in the body.")
    void updateCompanyToActive_200() throws Exception {
        var company = TestUtils.getSampleInactiveCompanyWithId(1L);
        var companyDTO = companyMapper.toDTO(company);
        when(companyService.updateToActive(any(Long.class))).thenReturn(company);
        mockMvc.perform(patch(TestConstants.API_V1_COMPANY_1_ACTIVE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtils.convertToJSON(companyDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(company.getId().intValue())))
                .andExpect(jsonPath("$.name", is(company.getName())))
                .andExpect(jsonPath("$.ticker", is(company.getTicker())))
                .andExpect(jsonPath("$.stockPrice", is(company.getStockPrice().intValue())))
                .andExpect(jsonPath("$.status", is(company.getStatus())));
    }

    @Test
    @DisplayName("Quando o método updateCompanyToInactive() é chamado com um ID válido, então deve retornar o status code 200 com a empresa atualizada no body." +
            "When the method updateCompanyToInactive() is called with a valid ID, then it should be return status code 200 with the company updated in the body.")
    void updateCompanyToInactive_200() throws Exception {
        var company = TestUtils.getSampleCompanyWithId(1L);
        var companyDTO = companyMapper.toDTO(company);
        when(companyService.updateToInactive(any(Long.class))).thenReturn(company);
        mockMvc.perform(patch(TestConstants.API_V1_COMPANY_1_INACTIVE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtils.convertToJSON(companyDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(company.getId().intValue())))
                .andExpect(jsonPath("$.name", is(company.getName())))
                .andExpect(jsonPath("$.ticker", is(company.getTicker())))
                .andExpect(jsonPath("$.stockPrice", is(company.getStockPrice().intValue())))
                .andExpect(jsonPath("$.status", is(company.getStatus())));
    }

    // Section: DELETE
    @Test
    @DisplayName("Quando o método deleteCompany() é chamado com um ID válido, então deve retornar o status code 200." +
            "When the method deleteCompany() is called with a valid ID, then it should be return status code 200.")
    void deleteCompany_200() throws Exception {
        doNothing().when(companyService).deleteById(any(Long.class));
        mockMvc.perform(delete(TestConstants.API_V1_COMPANY_1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Quando o método deleteCompany() é chamado com um ID inválido, então deve retornar o status code 404." +
            "When the method deleteCompany() is called with a invalid ID, then it should be return status code 404.")
    void deleteCompany_404() throws Exception {
        doThrow(CompanyNotFoundException.class).when(companyService).deleteById(any(Long.class));
        mockMvc.perform(delete(TestConstants.API_V1_COMPANY_1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
