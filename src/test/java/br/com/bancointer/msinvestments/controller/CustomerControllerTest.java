package br.com.bancointer.msinvestments.controller;

import br.com.bancointer.msinvestments.domain.dto.CustomerDTO;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerConflictDetectedException;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerNotFoundException;
import br.com.bancointer.msinvestments.mapper.CustomerMapper;
import br.com.bancointer.msinvestments.service.CustomerService;
import br.com.bancointer.msinvestments.test.TestConstants;
import br.com.bancointer.msinvestments.test.TestUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(CustomerController.class)
class CustomerControllerTest {

    private final CustomerMapper customerMapper = CustomerMapper.INSTANCE;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerService customerService;

    @InjectMocks
    private CustomerController customerController;

    // Section: SAVE
    @Test
    @DisplayName("Quando o método saveCustomer() é chamado com um CustomerDTO válido, então deve retornar o status code 200 com a cliente criada no body." +
            "When the method saveCustomer() is called with a valid CustomerDTO, then it should be return status code 200 with the customer created in the body.")
    void saveCustomer_201() throws Exception {
        var customer = TestUtils.getSampleCustomerWithoutId();
        var customerDTO = customerMapper.toDTO(customer);
        when(customerService.save(any(CustomerDTO.class))).thenReturn(customer);
        mockMvc.perform(post(TestConstants.API_V1_CUSTOMER)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtils.convertToJSON(customerDTO)))
                        .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(customerDTO.getName())))
                .andExpect(jsonPath("$.cpf", is(customerDTO.getCpf())));
    }

    @Test
    @DisplayName("Quando o método saveCustomer() é chamado com um CustomerDTO inválido, então deve retornar o status code 400." +
            "When the method saveCustomer() is called with a invalid CustomerDTO, then it should be return status code 400.")
    void saveCustomer_400() throws Exception {
        var customer = TestUtils.getEmptyCustomer();
        var customerDTO = customerMapper.toDTO(customer);
        mockMvc.perform(post(TestConstants.API_V1_CUSTOMER)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtils.convertToJSON(customerDTO)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Quando o método saveCustomer() é chamado com um CustomerDTO em conflito, então deve retornar o status code 409." +
            "When the method saveCustomer() is called with a conflicted CustomerDTO, then it should be return status code 409.")
    void saveCustomer_409() throws Exception {
        var customer = TestUtils.getSampleCustomerWithId(1L);
        var customerDTO = customerMapper.toDTO(customer);
        when(customerService.save(any(CustomerDTO.class))).thenThrow(CustomerConflictDetectedException.class);
        mockMvc.perform(post(TestConstants.API_V1_CUSTOMER)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtils.convertToJSON(customerDTO)))
                .andExpect(status().isConflict());
    }

    // Section: GET ALL
    @Test
    @DisplayName("Quando o método getAllCustomers() é chamado com uma propriedade válida (ex. Pageable), então deve retornar o status code 200 com a lista de clientes encontradas no body." +
            "When the method getAllCustomers() is called with a valid property (i.e. Pageable), then it should be return status code 200 with the list of customers found in the body.")
    void getAllCustomers_200() throws Exception {
        var customerToCompare = TestUtils.getSampleCustomerWithId(1L);
        var customerList = TestUtils.getSamplePageCustomer();
        when(customerService.findAll(any(Pageable.class))).thenReturn(customerList);
        mockMvc.perform(get(TestConstants.API_V1_CUSTOMER)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].id", is(customerToCompare.getId().intValue())))
                .andExpect(jsonPath("$.content[0].name", is(customerToCompare.getName())))
                .andExpect(jsonPath("$.content[0].cpf", is(customerToCompare.getCpf())));
    }

    @Test
    @DisplayName("Quando o método getAllCustomers() é chamado, mas não existe nenhuma cliente no banco de dados, então deve retornar o status code 404." +
            "When the method getAllCustomers() is called, but there are no customers in the database, then it should be return status code 404.")
    void getAllCustomers_404() throws Exception {
        when(customerService.findAll(any(Pageable.class))).thenThrow(CustomerNotFoundException.class);
        mockMvc.perform(get(TestConstants.API_V1_CUSTOMER)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    // Section: GET ONE
    @Test
    @DisplayName("Quando o método getOneCustomer() é chamado com um ID válido, então deve retornar o status code 200 com a cliente encontrada no body." +
            "When the method getOneCustomer() is called with a valid ID, then it should be return status code 200 with the customer found in the body.")
    void getOneCustomer_200() throws Exception {
        var customerToCompare = TestUtils.getSampleCustomerWithId(1L);
        when(customerService.findById(any(Long.class))).thenReturn(customerToCompare);
        mockMvc.perform(get(TestConstants.API_V1_CUSTOMER_1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(customerToCompare.getId().intValue())))
                .andExpect(jsonPath("$.name", is(customerToCompare.getName())))
                .andExpect(jsonPath("$.cpf", is(customerToCompare.getCpf())));
    }

    @Test
    @DisplayName("Quando o método getOneCustomer() é chamado com um ID inválido, então deve retornar o status code 404." +
            "When the method getOneCustomer() is called with a invalid ID, then it should be return status code 404.")
    void getOneCustomer_404() throws Exception {
        when(customerService.findById(any(Long.class))).thenThrow(CustomerNotFoundException.class);
        mockMvc.perform(get(TestConstants.API_V1_CUSTOMER_1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    // Section: UPDATE
    @Test
    @DisplayName("Quando o método updateCustomer() é chamado com um ID válido, então deve retornar o status code 200 com a cliente atualizada no body." +
            "When the method updateCustomer() is called with a valid ID and CustomerDTO, then it should be return status code 200 with the customer updated in the body.")
    void updateCustomer_200() throws Exception {
        var customer = TestUtils.getSampleCustomerWithId(1L);
        var customerDTO = customerMapper.toDTO(customer);
        when(customerService.update(any(Long.class), any(CustomerDTO.class))).thenReturn(customer);
        mockMvc.perform(put(TestConstants.API_V1_CUSTOMER_1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtils.convertToJSON(customerDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(customer.getId().intValue())))
                .andExpect(jsonPath("$.name", is(customer.getName())))
                .andExpect(jsonPath("$.cpf", is(customer.getCpf())));
    }

    @Test
    @DisplayName("Quando o método updateCustomer() é chamado com um ID inválido, então deve retornar o status code 404." +
            "When the method updateCustomer() is called with a invalid ID or CustomerDTO, then it should be return status code 404.")
    void updateCustomer_404() throws Exception {
        var customer = TestUtils.getSampleCustomerWithId(1L);
        var customerDTO = customerMapper.toDTO(customer);
        when(customerService.update(any(Long.class), any(CustomerDTO.class))).thenThrow(CustomerNotFoundException.class);
        mockMvc.perform(put(TestConstants.API_V1_CUSTOMER_1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtils.convertToJSON(customerDTO)))
                .andExpect(status().isNotFound());
    }

    // Section: DELETE
    @Test
    @DisplayName("Quando o método deleteCustomer() é chamado com um ID válido, então deve retornar o status code 200." +
            "When the method deleteCustomer() is called with a valid ID, then it should be return status code 200.")
    void deleteCustomer_200() throws Exception {
        doNothing().when(customerService).deleteById(any(Long.class));
        mockMvc.perform(delete(TestConstants.API_V1_CUSTOMER_1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Quando o método deleteCustomer() é chamado com um ID inválido, então deve retornar o status code 404." +
            "When the method deleteCustomer() is called with a invalid ID, then it should be return status code 404.")
    void deleteCustomer_404() throws Exception {
        doThrow(CustomerNotFoundException.class).when(customerService).deleteById(any(Long.class));
        mockMvc.perform(delete(TestConstants.API_V1_CUSTOMER_1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
