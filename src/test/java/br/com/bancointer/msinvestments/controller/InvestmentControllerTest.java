package br.com.bancointer.msinvestments.controller;

import br.com.bancointer.msinvestments.domain.dto.request.InvestmentRequest;
import br.com.bancointer.msinvestments.domain.exception.investment.InsuficientTotalToInvestException;
import br.com.bancointer.msinvestments.domain.exception.investment.InvestmentNotFoundException;
import br.com.bancointer.msinvestments.service.InvestmentService;
import br.com.bancointer.msinvestments.test.TestConstants;
import br.com.bancointer.msinvestments.test.TestUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(InvestmentController.class)
class InvestmentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private InvestmentService investmentService;

    @InjectMocks
    private InvestmentController investmentController;

    // Section: SAVE
    @Test
    @DisplayName("Quando o método saveInvestment() é chamado com um InvestmentRequest válido, então deve retornar o status code 201 com o investimento criado no body." +
            "When the method saveInvestment() is called with a valid InvestmentRequest, then it should be return status code 201 with the investment created in the body.")
    void saveInvestment_201() throws Exception {
        var investment = TestUtils.getSampleInvestmentWithId(1L);
        var investmentRequest = TestUtils.getSampleInvestmentRequest(investment);
        var investmentResponse = TestUtils.getSampleInvestmentResponse(investment);
        when(investmentService.save(any(InvestmentRequest.class))).thenReturn(investmentResponse);
        mockMvc.perform(post(TestConstants.API_V1_INVESTMENT)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtils.convertToJSON(investmentRequest)))
                        .andExpect(status().isCreated())
                .andExpect(jsonPath("$.investmentsMade[0].id", is(investmentResponse.getInvestmentsMade().get(0).getId().intValue())))
                .andExpect(jsonPath("$.amount", is(investmentResponse.getAmount().intValue())))
                .andExpect(jsonPath("$.change", is(investmentResponse.getChange().intValue())));
    }

    @Test
    @DisplayName("Quando o método saveInvestment() é chamado, mas o total a investir é insuficiente, então uma exceção deve ser lançada." +
            "When the method saveInvestment() is called, but the total to be invested is insufficient, then a exception should be thrown.")
    void saveInvestment_400() throws Exception {
        var investmentRequest = TestUtils.getSampleInvestmentRequest(TestUtils.getSampleInvestmentWithId(1L));
        when(investmentService.save(any(InvestmentRequest.class))).thenThrow(InsuficientTotalToInvestException.class);
        mockMvc.perform(post(TestConstants.API_V1_INVESTMENT)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtils.convertToJSON(investmentRequest)))
                .andExpect(status().isBadRequest());
    }

    // Section: GET ALL
    @Test
    @DisplayName("Quando o método getAllInvestments() é chamado com os parâmetros válidos (ex. Pageable, customerId, companyId), então deve retornar o status code 200 com a lista de investimentos encontradas no body." +
            "When the method getAllInvestments() is called with a valid paramenters (i.e. Pageable, customerId, companyId), then it should be return status code 200 with the list of investments found in the body.")
    void getAllInvestments_200() throws Exception {
        var investment = TestUtils.getSampleInvestmentWithId(1L);
        var investmentPage = TestUtils.getSamplePageInvestmentWithId(investment);
        when(investmentService.findAllByCustomerIdAndCompanyId(any(Pageable.class), any(Long.class), any(Long.class))).thenReturn(investmentPage);
        mockMvc.perform(get(TestConstants.API_V1_INVESTMENT)
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("customerId", investment.getCustomer().getId().toString())
                        .param("companyId", investment.getCompany().getId().toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].id", is(investment.getId().intValue())))
                .andExpect(jsonPath("$.content[0].company.id", is(investment.getCompany().getId().intValue())))
                .andExpect(jsonPath("$.content[0].numberOfShares", is(investment.getNumberOfShares())))
                .andExpect(jsonPath("$.content[0].total", is(investment.getTotal().intValue())))
                .andExpect(jsonPath("$.content[0].change", is(investment.getChange().intValue())));
    }

    // Section: GET ONE
    @Test
    @DisplayName("Quando o método getOneInvestment() é chamado com um ID válido, então deve retornar o status code 200 com o investimento encontrado no body." +
            "When the method getOneInvestment() is called with a valid ID, then it should be return status code 200 with the investment found in the body.")
    void getOneInvestment_200() throws Exception {
        var investmentToCompare = TestUtils.getSampleInvestmentWithId(1L);
        when(investmentService.findById(any(Long.class))).thenReturn(investmentToCompare);
        mockMvc.perform(get(TestConstants.API_V1_INVESTMENT_1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(investmentToCompare.getId().intValue())))
                .andExpect(jsonPath("$.company.id", is(investmentToCompare.getCompany().getId().intValue())))
                .andExpect(jsonPath("$.numberOfShares", is(investmentToCompare.getNumberOfShares())))
                .andExpect(jsonPath("$.total", is(investmentToCompare.getTotal().intValue())))
                .andExpect(jsonPath("$.change", is(investmentToCompare.getChange().intValue())));
    }

    @Test
    @DisplayName("Quando o método getOneInvestment() é chamado com um ID inválido, então uma exceção deve ser lançada." +
            "When the method getOneInvestment() is called with a invalid ID, then a exception should be thrown.")
    void getOneInvestment_404() throws Exception {
        when(investmentService.findById(any(Long.class))).thenThrow(InvestmentNotFoundException.class);
        mockMvc.perform(get(TestConstants.API_V1_INVESTMENT_1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
