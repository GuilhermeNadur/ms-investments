package br.com.bancointer.msinvestments.dto;

import br.com.bancointer.msinvestments.domain.dto.ErrorMessageDTO;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = { ErrorMessageDTO.class })
public class ErrorMessageDTOTest {

    @Test
    void noArgsConstructor() {
        assertNotNull(ErrorMessageDTO.builder().build());
    }
}