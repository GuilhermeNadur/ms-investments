package br.com.bancointer.msinvestments.dto;

import br.com.bancointer.msinvestments.domain.dto.SuccessMessageDTO;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = { SuccessMessageDTO.class })
public class SuccessMessageDTOTest {

    @Test
    void noArgsConstructor() {
        assertNotNull(SuccessMessageDTO.builder().build());
    }
}