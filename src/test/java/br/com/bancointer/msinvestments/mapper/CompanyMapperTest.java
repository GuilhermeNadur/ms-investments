package br.com.bancointer.msinvestments.mapper;

import br.com.bancointer.msinvestments.test.TestUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest(classes = { CompanyMapperImpl.class })
public class CompanyMapperTest {

    private final CompanyMapper companyMapper = CompanyMapper.INSTANCE;

    @Test
    @DisplayName("Quando o método toDTO() é chamado com um Company válido, então deve retornar o CompanyDTO." +
            "When the method toDTO() is called with a valid Company, then it should be return the CompanyDTO.")
    void toDTO_success() {
        var companyEntity = TestUtils.getSampleCompanyWithId(1L);
        var companyDTO = companyMapper.toDTO(companyEntity);
        assertNotNull(companyDTO);
        assertThat(companyDTO.getName(), is(equalTo(companyEntity.getName())));
        assertThat(companyDTO.getTicker(), is(equalTo(companyEntity.getTicker())));
        assertThat(companyDTO.getStockPrice(), is(equalTo(companyEntity.getStockPrice())));
        assertThat(companyDTO.getStatus(), is(equalTo(companyEntity.getStatus())));
    }

    @Test
    @DisplayName("Quando o método toDTO() é chamado com um Company nulo, então deve retornar nulo." +
            "When the method toDTO() is called with a null Company, then it should be return null.")
    void toDTO_fail() {
        var companyDTO = companyMapper.toDTO(null);
        assertNull(companyDTO);
    }

    @Test
    @DisplayName("Quando o método toModel() é chamado com um CompanyDTO válido, então deve retornar o Company." +
            "When the method toModel() is called with a valid CompanyDTO, then it should be return the Company.")
    void toModel_success() {
        var companyDTO = TestUtils.getSampleCompanyDTO();
        var companyEntity = companyMapper.toModel(companyDTO);
        assertNotNull(companyEntity);
        assertThat(companyEntity.getName(), is(equalTo(companyDTO.getName())));
        assertThat(companyEntity.getTicker(), is(equalTo(companyDTO.getTicker())));
        assertThat(companyEntity.getStockPrice(), is(equalTo(companyDTO.getStockPrice())));
        assertThat(companyEntity.getStatus(), is(equalTo(companyDTO.getStatus())));
    }

    @Test
    @DisplayName("Quando o método toModel() é chamado com um CompanyDTO nulo, então deve retornar nulo." +
            "When the method toModel() is called with a null CompanyDTO, then it should be return null.")
    void toModel_fail() {
        var companyEntity = companyMapper.toModel(null);
        assertNull(companyEntity);
    }
}
