package br.com.bancointer.msinvestments.mapper;

import br.com.bancointer.msinvestments.test.TestUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest(classes = { CustomerMapperImpl.class })
public class CustomerMapperTest {

    private final CustomerMapper customerMapper = CustomerMapper.INSTANCE;

    @Test
    @DisplayName("Quando o método toDTO() é chamado com um Customer válido, então deve retornar o CustomerDTO." +
            "When the method toDTO() is called with a valid Customer, then it should be return the CustomerDTO.")
    void toDTO_success() {
        var customerEntity = TestUtils.getSampleCustomerWithId(1L);
        var customerDTO = customerMapper.toDTO(customerEntity);
        assertNotNull(customerDTO);
        assertThat(customerDTO.getName(), is(equalTo(customerEntity.getName())));
        assertThat(customerDTO.getCpf(), is(equalTo(customerEntity.getCpf())));
        assertThat(customerDTO.getInvestments(), is(equalTo(customerEntity.getInvestments())));
    }

    @Test
    @DisplayName("Quando o método toDTO() é chamado com um Customer nulo, então deve retornar nulo." +
            "When the method toDTO() is called with a null Customer, then it should be return null.")
    void toDTO_fail() {
        var customerDTO = customerMapper.toDTO(null);
        assertNull(customerDTO);
    }

    @Test
    @DisplayName("Quando o método toModel() é chamado com um CustomerDTO válido, então deve retornar o Customer." +
            "When the method toModel() is called with a valid CustomerDTO, then it should be return the Customer.")
    void toModel_success() {
        var customerDTO = TestUtils.getSampleCustomerDTO();
        var customerEntity = customerMapper.toModel(customerDTO);
        assertNotNull(customerEntity);
        assertThat(customerEntity.getName(), is(equalTo(customerDTO.getName())));
        assertThat(customerEntity.getCpf(), is(equalTo(customerDTO.getCpf())));
        assertThat(customerEntity.getInvestments(), is(equalTo(customerDTO.getInvestments())));
    }

    @Test
    @DisplayName("Quando o método toModel() é chamado com um CustomerDTO nulo, então deve retornar nulo." +
            "When the method toModel() is called with a null CustomerDTO, then it should be return null.")
    void toModel_fail() {
        var customerEntity = customerMapper.toModel(null);
        assertNull(customerEntity);
    }
}
