package br.com.bancointer.msinvestments.repository;

import br.com.bancointer.msinvestments.test.TestConstants;
import br.com.bancointer.msinvestments.test.TestUtils;
import br.com.bancointer.msinvestments.util.PageUtils;
import lombok.AllArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CompanyRepositoryTest {

    private final CompanyRepository companyRepository;

    @BeforeEach
    void tearDown() {
        companyRepository.deleteAll();
    }

    @Test
    @DisplayName("Quando o método findAllByStatus() é chamado com um status válido, então deve retornar uma lista de empresas no body." +
            "When the method findAllByStatus() is called with valid status, then it should be return a company list in the body.")
    void findAllByStatus_success() {
        var activeCompany = TestUtils.getSampleCompanyWithoutId();
        var inactiveCompany = TestUtils.getSampleInactiveCompanyWithoutId();
        companyRepository.saveAll(Arrays.asList(activeCompany, inactiveCompany));
        var valueExpected = companyRepository.findAllByStatus(Boolean.TRUE);
        assertEquals(valueExpected.get(0), activeCompany);
        assertThrows(IndexOutOfBoundsException.class, () -> valueExpected.get(1));
    }

    @Test
    @DisplayName("Quando o método findAllByStatus() é chamado com um status e um Pageable válido, então deve retornar uma lista de empresas no body." +
            "When the method findAllByStatus() is called with valid status and Pageable, then it should be return a company list in the body.")
    void findAllByStatusWithPageable_success() {
        var activeCompany = TestUtils.getSampleCompanyWithoutId();
        var inactiveCompany = TestUtils.getSampleInactiveCompanyWithoutId();
        companyRepository.saveAll(Arrays.asList(activeCompany, inactiveCompany));
        var valueExpected = companyRepository.findAllByStatus(PageUtils.getDefaultPageRequest(), Boolean.TRUE);
        assertEquals(valueExpected.getContent().get(0), activeCompany);
        assertThrows(IndexOutOfBoundsException.class, () -> valueExpected.getContent().get(1));
    }

    @Test
    @DisplayName("Quando o método existsByTicker() é chamado com um ticker existente, então deve retornar um valor booleano true." +
            "When the method existsByTicker() is called with existing ticker, then it should be return a true boolean value.")
    void existsByTicker_true() {
        var companyTest = TestUtils.getSampleCompanyWithoutId();
        companyRepository.save(companyTest);
        var valueExpected = companyRepository.existsByTicker(companyTest.getTicker());
        assertTrue(valueExpected);
    }

    @Test
    @DisplayName("Quando o método existsByTicker() é chamado com um ticker inexistente, então deve retornar um valor booleano false." +
            "When the method existsByTicker() is called with non existing ticker, then it should be return a false boolean value.")
    void existsByTicker_false() {
        var valueExpected = companyRepository.existsByTicker(TestConstants.NON_EXISTENT_TICKER);
        assertFalse(valueExpected);
    }
}
