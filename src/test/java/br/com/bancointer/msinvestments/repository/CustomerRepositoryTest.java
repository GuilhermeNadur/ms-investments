package br.com.bancointer.msinvestments.repository;

import br.com.bancointer.msinvestments.test.TestConstants;
import br.com.bancointer.msinvestments.test.TestUtils;
import lombok.AllArgsConstructor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CustomerRepositoryTest {

    private final CustomerRepository customerRepository;

    @AfterEach
    void tearDown() {
        customerRepository.deleteAll();
    }

    @Test
    @DisplayName("Quando o método findByCpf() é chamado com um CPF válido, então deve retornar um valor booleano true." +
            "When the method findByCpf() is called with a valid CPF, then it should be return a true boolean value.")
    void findByCpf_success() {
        var customerTest = TestUtils.getSampleCustomerWithoutId();
        customerRepository.save(customerTest);
        var valueExpected = customerRepository.findByCpf(customerTest.getCpf());
        assertEquals(valueExpected.get(), customerTest);
    }

    @Test
    @DisplayName("Quando o método findByCpf() é chamado com um CPF inválido, então uma exceção deve ser lançada." +
            "When the method findByCpf() is called with a invalid CPF, then a exception should be thrown.")
    void findByCpf_fail() {
        var valueExpected = customerRepository.findByCpf(TestConstants.INVALID_CPF);
        assertEquals(valueExpected, Optional.empty());
    }

    @Test
    @DisplayName("Quando o método existsByCpf() é chamado com um CPF válido, então deve retornar um valor booleano true." +
            "When the method existsByCpf() is called with a valid CPF, then it should be return a true boolean value.")
    void existsByCpf_true() {
        var customerTest = TestUtils.getSampleCustomerWithoutId();
        customerRepository.save(customerTest);
        var valueExpected = customerRepository.existsByCpf(customerTest.getCpf());
        assertTrue(valueExpected);
    }

    @Test
    @DisplayName("Quando o método existsByCpf() é chamado com um CPF inválido, então deve retornar um valor booleano false." +
            "When the method existsByCpf() is called with a invalid CPF, then it should be return a false boolean value.")
    void existsByCpf_false() {
        var valueExpected = customerRepository.existsByCpf(TestConstants.INVALID_CPF);
        assertFalse(valueExpected);
    }
}
