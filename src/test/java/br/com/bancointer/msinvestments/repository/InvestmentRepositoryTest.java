package br.com.bancointer.msinvestments.repository;

import br.com.bancointer.msinvestments.domain.Company;
import br.com.bancointer.msinvestments.domain.Customer;
import br.com.bancointer.msinvestments.test.TestUtils;
import br.com.bancointer.msinvestments.util.PageUtils;
import lombok.AllArgsConstructor;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@AllArgsConstructor(onConstructor = @__(@Autowired))
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class InvestmentRepositoryTest {

    private final InvestmentRepository investmentRepository;
    private final CustomerRepository customerRepository;
    private final CompanyRepository companyRepository;

    private static Customer sampleCustomer;
    private static Company sampleCompany;

    @BeforeAll
    static void setUp() {
        sampleCustomer = TestUtils.getSampleCustomerWithoutId();
        sampleCompany = TestUtils.getSampleCompanyWithoutId();
    }

    @BeforeEach
    void tearDown() {
        investmentRepository.deleteAll();
        customerRepository.deleteAll();
        companyRepository.deleteAll();
    }

    @Test
    @Order(1)
    @DisplayName("Quando o método findAllByCustomerIdAndCompanyId() é chamado com um customerId e companyId válidos, então deve retornar uma lista de investimentos no body." +
            "When the method findAllByCustomerIdAndCompanyId() is called with a valid customerId and companyId, then it should be return a investment list in the body.")
    void findAllByCustomerIdAndCompanyId_success() {
        sampleCustomer = customerRepository.save(sampleCustomer);
        sampleCompany = companyRepository.save(sampleCompany);
        var investment = TestUtils.getPersonalizedInvestment(sampleCustomer, sampleCompany);
        var pageInvestment = TestUtils.getSamplePageInvestmentWithId(investment);
        investmentRepository.save(investment);
        var valueExpected = investmentRepository.findAllByCustomerIdAndCompanyId(PageUtils.getDefaultPageRequest(), sampleCustomer.getId(), sampleCompany.getId());
        assertEquals(pageInvestment.getContent().get(0).getCustomer(), valueExpected.getContent().get(0).getCustomer());
        assertEquals(pageInvestment.getContent().get(0).getCompany(), valueExpected.getContent().get(0).getCompany());
        assertEquals(pageInvestment.getContent().get(0), valueExpected.getContent().get(0));
    }

    @Test
    @Order(2)
    @DisplayName("Quando o método findAllByCustomerId() é chamado com um customerId válido, então deve retornar uma lista de investimentos no body." +
            "When the method findAllByCustomerId() is called with a valid customerId, then it should be return a investment list in the body.")
    void findAllByCustomerId_success() {
        sampleCustomer = customerRepository.save(sampleCustomer);
        sampleCompany = companyRepository.save(sampleCompany);
        var investment = TestUtils.getPersonalizedInvestment(sampleCustomer, sampleCompany);
        var pageInvestment = TestUtils.getSamplePageInvestmentWithId(investment);
        customerRepository.save(investment.getCustomer());
        companyRepository.save(investment.getCompany());
        investmentRepository.save(investment);
        var valueExpected = investmentRepository.findAllByCustomerId(PageUtils.getDefaultPageRequest(), sampleCustomer.getId());
        assertEquals(pageInvestment.getContent().get(0).getCustomer(), valueExpected.getContent().get(0).getCustomer());
        assertEquals(pageInvestment.getContent().get(0).getCompany(), valueExpected.getContent().get(0).getCompany());
        assertEquals(pageInvestment.getContent().get(0), valueExpected.getContent().get(0));
    }

    @Test
    @Order(3)
    @DisplayName("Quando o método findAllByCompanyId() é chamado somente com um companyId válido, então deve retornar uma lista de investimentos no body." +
            "When the method findAllByCompanyId() is called with a only valid companyId, then it should be return a investment list in the body.")
    void findAllByCompanyId_success() {
        sampleCustomer = customerRepository.save(sampleCustomer);
        sampleCompany = companyRepository.save(sampleCompany);
        var investment = TestUtils.getPersonalizedInvestment(sampleCustomer, sampleCompany);
        var pageInvestment = TestUtils.getSamplePageInvestmentWithId(investment);
        customerRepository.save(investment.getCustomer());
        companyRepository.save(investment.getCompany());
        investmentRepository.save(investment);
        var valueExpected = investmentRepository.findAllByCompanyId(PageUtils.getDefaultPageRequest(), sampleCompany.getId());
        assertEquals(pageInvestment.getContent().get(0).getCustomer(), valueExpected.getContent().get(0).getCustomer());
        assertEquals(pageInvestment.getContent().get(0).getCompany(), valueExpected.getContent().get(0).getCompany());
        assertEquals(pageInvestment.getContent().get(0), valueExpected.getContent().get(0));
    }
}
