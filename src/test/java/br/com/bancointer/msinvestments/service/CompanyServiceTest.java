package br.com.bancointer.msinvestments.service;

import br.com.bancointer.msinvestments.domain.Company;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyConflictDetectedException;
import br.com.bancointer.msinvestments.domain.exception.company.CompanyNotFoundException;
import br.com.bancointer.msinvestments.mapper.CompanyMapper;
import br.com.bancointer.msinvestments.repository.CompanyRepository;
import br.com.bancointer.msinvestments.service.impl.CompanyServiceImpl;
import br.com.bancointer.msinvestments.test.TestUtils;
import br.com.bancointer.msinvestments.util.PageUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CompanyServiceTest {

    private final CompanyMapper companyMapper = CompanyMapper.INSTANCE;

    @Mock
    private CompanyRepository companyRepository;

    @InjectMocks
    private CompanyServiceImpl companyService;

    // Section: SAVE
    @Test
    @DisplayName("Quando o método save() é chamado com um Company válido, então deve retornar a Company criada com o link do HATEOAS." +
            "When the method save() is called with a valid Company, then it should be return the Company created with the HATEOAS link.")
    void save_success() throws CompanyConflictDetectedException, CompanyNotFoundException {
        var actualCompanyToSave = TestUtils.getSampleCompanyWithoutId();
        when(companyRepository.save(actualCompanyToSave)).thenReturn(actualCompanyToSave);
        var expectedCompanySaved = companyService.save(companyMapper.toDTO(actualCompanyToSave));
        assertThat(actualCompanyToSave, is(equalTo(expectedCompanySaved)));
        assertThat(actualCompanyToSave.getLinks(), is(equalTo(expectedCompanySaved.getLinks())));
    }

    @Test
    @DisplayName("Quando o método save() é chamado com um Company inválido, então uma exceção deve ser lançada." +
            "When the method save() is called with a invalid Company, then a exception should be thrown.")
    void save_fail() {
        var actualCompanyToSave = TestUtils.getSampleCompanyWithId(1L);
        when(companyRepository.existsByTicker(actualCompanyToSave.getTicker())).thenReturn(Boolean.TRUE);
        assertThrows(CompanyConflictDetectedException.class, () -> companyService.save(companyMapper.toDTO(actualCompanyToSave)));
    }

    // Section: FIND ALL
    @Test
    @DisplayName("Quando o método findAll() é chamado com Pageable padrão e há empresas no banco de dados, então deve retornar um Page<Company> com o link do HATEOAS." +
            "When the method findAll() is called with default Pageable and there are companies in the database, then it should be return a Page<Company> with the HATEOAS link.")
    void findAll_success() throws CompanyNotFoundException {
        var actualPageCompany = TestUtils.getSamplePageCompany();
        when(companyRepository.findAll(any(Pageable.class))).thenReturn(actualPageCompany);
        var expectedPageCompany = companyService.findAll(PageUtils.getDefaultPageRequest());
        verify(companyRepository).findAll(any(Pageable.class));
        assertThat(actualPageCompany, is(not(emptyIterableOf(Company.class))));
        assertThat(actualPageCompany, is(equalTo(expectedPageCompany)));
        assertThat(actualPageCompany.getContent().get(0).getLinks(), is(equalTo(expectedPageCompany.getContent().get(0).getLinks())));
    }

    @Test
    @DisplayName("Quando o método findAll() é chamado há não empresas no banco de dados, então uma exceção deve ser lançada." +
            "When the method findAll() is called and there are not companies in the database, then a exception should be thrown.")
    void findAll_fail() {
        var defaultPageResquest = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
        when(companyRepository.findAll(defaultPageResquest)).thenReturn(Page.empty());
        assertThrows(CompanyNotFoundException.class, () -> companyService.findAll(defaultPageResquest));
    }

    @Test
    @DisplayName("Quando o método findAllByStatus() é chamado com Pageable e boolean válidos, então deve retornar um Page<Company>." +
            "When the method findAllByStatus() is called with valid Pageable and boolean, then it should be return a Page<Company>.")
    void findAllByStatus_success() throws CompanyNotFoundException {
        var actualPageCompany = TestUtils.getSamplePageCompany();
        when(companyRepository.findAllByStatus(any(Pageable.class), any(Boolean.class))).thenReturn(actualPageCompany);
        var expectedPageCompany = companyService.findAllByStatus(PageUtils.getDefaultPageRequest(), Boolean.TRUE);
        verify(companyRepository).findAllByStatus(any(Pageable.class), any(Boolean.class));
        assertThat(actualPageCompany, is(not(emptyIterableOf(Company.class))));
        assertThat(actualPageCompany, is(equalTo(expectedPageCompany)));
        assertThat(actualPageCompany.getContent().get(0).getLinks(), is(equalTo(expectedPageCompany.getContent().get(0).getLinks())));
    }

    // Section: FIND BY ID
    @Test
    @DisplayName("Quando o método findById() é chamado com ID existente, então deve retornar a Company encontrada com o link do HATEOAS." +
            "When the method findById() is called with existing ID, then it should be return the Company found with the HATEOAS link.")
    void findById_success() throws CompanyNotFoundException {
        var actualCompanyToFind = TestUtils.getSampleCompanyWithId(1L);
        when(companyRepository.findById(actualCompanyToFind.getId())).thenReturn(Optional.of(actualCompanyToFind));
        var expectedCompanyFound = companyService.findById(actualCompanyToFind.getId());
        assertThat(actualCompanyToFind, is(equalTo(expectedCompanyFound)));
        assertThat(actualCompanyToFind.getLinks(), is(equalTo(expectedCompanyFound.getLinks())));
    }

    @Test
    @DisplayName("Quando o método findById() é chamado com ID inexistente, então uma exceção deve ser lançada." +
            "When the method findById() is called with non existent ID, then a exception should be thrown.")
    void findById_fail() {
        var actualCompanyToFind = TestUtils.getSampleCustomerWithId(1L);
        when(companyRepository.findById(actualCompanyToFind.getId())).thenReturn(Optional.empty());
        assertThrows(CompanyNotFoundException.class, () -> companyService.findById(actualCompanyToFind.getId()));
    }

    // Section: UPDATE
    @Test
    @DisplayName("Quando o método update() é chamado com ID existente, então deve retornar a Company atualizada com o link do HATEOAS." +
            "When the method update() is called with existing ID, then it should be return the Company updated with the HATEOAS link.")
    void update_success() throws CompanyNotFoundException {
        var actualCompanyToUpdate = TestUtils.getSampleCompanyWithId(1L);
        System.out.println(actualCompanyToUpdate.hashCode());
        when(companyRepository.findById(actualCompanyToUpdate.getId())).thenReturn(Optional.of(actualCompanyToUpdate));
        when(companyRepository.save(actualCompanyToUpdate)).thenReturn(actualCompanyToUpdate);
        var expectedCompanyUpdated = companyService.update(actualCompanyToUpdate.getId(), companyMapper.toDTO(actualCompanyToUpdate));
        assertThat(actualCompanyToUpdate, is(equalTo(expectedCompanyUpdated)));
        assertThat(actualCompanyToUpdate.getLinks(), is(equalTo(expectedCompanyUpdated.getLinks())));
    }

    @Test
    @DisplayName("Quando o método update() é chamado com ID inexistente, então uma exceção deve ser lançada." +
            "When the method update() is called with non existent ID, then a exception should be thrown.")
    void update_fail() {
        var actualCompanyToUpdate = TestUtils.getSampleCompanyWithId(1L);
        when(companyRepository.findById(actualCompanyToUpdate.getId())).thenReturn(Optional.empty());
        assertNull(actualCompanyToUpdate.getInvestments());
        assertThrows(CompanyNotFoundException.class, () -> companyService.update(actualCompanyToUpdate.getId(), companyMapper.toDTO(actualCompanyToUpdate)));
    }

    @Test
    @DisplayName("Quando o método updateToActive() é chamado com ID existente, então deve retornar a Company atualizada com o link do HATEOAS." +
            "When the method updateToActive() is called with existing ID, then it should be return the Company updated with the HATEOAS link.")
    void updateToActive_success() throws CompanyNotFoundException {
        var actualCompanyToUpdate = TestUtils.getSampleCompanyWithId(1L);
        when(companyRepository.findById(any(Long.class))).thenReturn(Optional.of(actualCompanyToUpdate));
        when(companyRepository.save(any(Company.class))).thenReturn(actualCompanyToUpdate);
        var expectedCompanyUpdated = companyService.updateToActive(actualCompanyToUpdate.getId());
        assertThat(actualCompanyToUpdate, is(equalTo(expectedCompanyUpdated)));
        assertThat(actualCompanyToUpdate.getLinks(), is(equalTo(expectedCompanyUpdated.getLinks())));
    }

    @Test
    @DisplayName("Quando o método updateToInactive() é chamado com ID existente, então deve retornar a Company atualizada com o link do HATEOAS." +
            "When the method updateToInactive() is called with existing ID, then it should be return the Company updated with the HATEOAS link.")
    void updateToInactive_success() throws CompanyNotFoundException {
        var actualCompanyToUpdate = TestUtils.getSampleCompanyWithId(1L);
        when(companyRepository.findById(any(Long.class))).thenReturn(Optional.of(actualCompanyToUpdate));
        when(companyRepository.save(any(Company.class))).thenReturn(actualCompanyToUpdate);
        var expectedCompanyUpdated = companyService.updateToInactive(actualCompanyToUpdate.getId());
        assertThat(actualCompanyToUpdate, is(equalTo(expectedCompanyUpdated)));
        assertThat(actualCompanyToUpdate.getLinks(), is(equalTo(expectedCompanyUpdated.getLinks())));
    }

    // Section: DELETE BY ID
    @Test
    @DisplayName("Quando o método deleteById() é chamado com ID existente, então a Company deve ser deletada e nada deve ser retornado." +
            "When the method deleteById() is called with existing ID, then the Company should be deleted and nothing should be return.")
    void deleteById_success() throws CompanyNotFoundException {
        var actualCompanyToDelete = TestUtils.getSampleCustomerWithId(1L);
        doNothing().when(companyRepository).deleteById(actualCompanyToDelete.getId());
        companyService.deleteById(actualCompanyToDelete.getId());
        verify(companyRepository, times(1)).deleteById(actualCompanyToDelete.getId());
    }
}
