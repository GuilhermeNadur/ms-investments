package br.com.bancointer.msinvestments.service;

import br.com.bancointer.msinvestments.domain.Customer;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerConflictDetectedException;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerNotFoundException;
import br.com.bancointer.msinvestments.mapper.CustomerMapper;
import br.com.bancointer.msinvestments.repository.CustomerRepository;
import br.com.bancointer.msinvestments.service.impl.CustomerServiceImpl;
import br.com.bancointer.msinvestments.test.TestUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {

    private final CustomerMapper customerMapper = CustomerMapper.INSTANCE;

    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private CustomerServiceImpl customerService;

    // Section: SAVE
    @Test
    @DisplayName("Quando o método save() é chamado com um Customer válido, então deve retornar a Customer criada com o link do HATEOAS." +
            "When the method save() is called with a valid Customer, then it should be return the Customer created with the HATEOAS link.")
    void save_success() throws CustomerConflictDetectedException, CustomerNotFoundException {
        var actualCustomerToSave = TestUtils.getSampleCustomerWithoutId();
        when(customerRepository.save(actualCustomerToSave)).thenReturn(actualCustomerToSave);
        var expectedCustomerSaved = customerService.save(customerMapper.toDTO(actualCustomerToSave));
        assertThat(actualCustomerToSave, is(equalTo(expectedCustomerSaved)));
        assertThat(actualCustomerToSave.getLinks(), is(equalTo(expectedCustomerSaved.getLinks())));
    }

    @Test
    @DisplayName("Quando o método save() é chamado com um Customer inválido, então uma exceção deve ser lançada." +
            "When the method save() is called with a invalid Customer, then a exception should be thrown.")
    void save_fail() {
        var actualCustomerToSave = TestUtils.getSampleCustomerWithId(1L);
        when(customerRepository.existsByCpf(actualCustomerToSave.getCpf())).thenReturn(Boolean.TRUE);
        assertThrows(CustomerConflictDetectedException.class, () -> customerService.save(customerMapper.toDTO(actualCustomerToSave)));
    }

    // Section: FIND ALL
    @Test
    @DisplayName("Quando o método findAll() é chamado com Pageable padrão e há clientes no banco de dados, então deve retornar um Page<Customer> com o link do HATEOAS." +
            "When the method findAll() is called with default Pageable and there are customers in the database, then it should be return a Page<Customer> with the HATEOAS link.")
    void findAll_success() throws CustomerNotFoundException {
        var defaultPageResquest = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
        var actualPageCustomer = TestUtils.getSamplePageCustomer(defaultPageResquest);
        when(customerRepository.findAll(defaultPageResquest)).thenReturn(actualPageCustomer);
        var expectedPageCustomer = customerService.findAll(defaultPageResquest);
        verify(customerRepository).findAll(defaultPageResquest);
        assertThat(actualPageCustomer, is(not(emptyIterableOf(Customer.class))));
        assertThat(actualPageCustomer, is(equalTo(expectedPageCustomer)));
        assertThat(actualPageCustomer.getContent().get(0).getLinks(), is(equalTo(expectedPageCustomer.getContent().get(0).getLinks())));
    }

    @Test
    @DisplayName("Quando o método findAll() é chamado há não clientes no banco de dados, então uma exceção deve ser lançada." +
            "When the method findAll() is called and there are not customers in the database, then a exception should be thrown.")
    void findAll_fail() {
        var defaultPageResquest = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
        when(customerRepository.findAll(defaultPageResquest)).thenReturn(Page.empty());
        assertThrows(CustomerNotFoundException.class, () -> customerService.findAll(defaultPageResquest));
    }

    // Section: FIND BY ID
    @Test
    @DisplayName("Quando o método findById() é chamado com ID existente, então deve retornar a Customer encontrada com o link do HATEOAS." +
            "When the method findById() is called with existing ID, then it should be return the Customer found with the HATEOAS link.")
    void findById_success() throws CustomerNotFoundException {
        var actualCustomerToFind = TestUtils.getSampleCustomerWithId(1L);
        when(customerRepository.findById(actualCustomerToFind.getId())).thenReturn(Optional.of(actualCustomerToFind));
        var expectedCustomerFound = customerService.findById(actualCustomerToFind.getId());
        assertThat(actualCustomerToFind, is(equalTo(expectedCustomerFound)));
        assertThat(actualCustomerToFind.getLinks(), is(equalTo(expectedCustomerFound.getLinks())));
    }

    @Test
    @DisplayName("Quando o método findById() é chamado com ID inexistente, então uma exceção deve ser lançada." +
            "When the method findById() is called with non existent ID, then a exception should be thrown.")
    void findById_fail() {
        var actualCustomerToFind = TestUtils.getSampleCustomerWithId(1L);
        when(customerRepository.findById(actualCustomerToFind.getId())).thenReturn(Optional.empty());
        assertThrows(CustomerNotFoundException.class, () -> customerService.findById(actualCustomerToFind.getId()));
    }

    // Section: UPDATE
    @Test
    @DisplayName("Quando o método update() é chamado com ID existente, então deve retornar a Customer atualizada com o link do HATEOAS." +
            "When the method update() is called with existing ID, then it should be return the Customer updated with the HATEOAS link.")
    void update_success() throws CustomerNotFoundException {
        var actualCustomerToUpdate = TestUtils.getSampleCustomerWithId(1L);
        when(customerRepository.findById(actualCustomerToUpdate.getId())).thenReturn(Optional.of(actualCustomerToUpdate));
        when(customerRepository.save(actualCustomerToUpdate)).thenReturn(actualCustomerToUpdate);
        var expectedCustomerUpdated = customerService.update(actualCustomerToUpdate.getId(), customerMapper.toDTO(actualCustomerToUpdate));
        assertThat(actualCustomerToUpdate, is(equalTo(expectedCustomerUpdated)));
        assertThat(actualCustomerToUpdate.getLinks(), is(equalTo(expectedCustomerUpdated.getLinks())));
    }

    @Test
    @DisplayName("Quando o método update() é chamado com ID inexistente, então uma exceção deve ser lançada." +
            "When the method update() is called with non existent ID, then a exception should be thrown.")
    void update_fail() {
        var actualCustomerToUpdate = TestUtils.getSampleCustomerWithId(1L);
        System.out.println(actualCustomerToUpdate.hashCode());
        when(customerRepository.findById(actualCustomerToUpdate.getId())).thenReturn(Optional.empty());
        assertThrows(CustomerNotFoundException.class, () -> customerService.update(actualCustomerToUpdate.getId(), customerMapper.toDTO(actualCustomerToUpdate)));
    }

    // Section: DELETE BY ID
    @Test
    @DisplayName("Quando o método deleteById() é chamado com ID existente, então a Customer deve ser deletada e nada deve ser retornado." +
            "When the method deleteById() is called with existing ID, then the Customer should be deleted and nothing should be return.")
    void deleteById_success() throws CustomerNotFoundException {
        var actualCustomerToDelete = TestUtils.getSampleCustomerWithId(1L);
        doNothing().when(customerRepository).deleteById(actualCustomerToDelete.getId());
        customerService.deleteById(actualCustomerToDelete.getId());
        verify(customerRepository, times(1)).deleteById(actualCustomerToDelete.getId());
    }
}
