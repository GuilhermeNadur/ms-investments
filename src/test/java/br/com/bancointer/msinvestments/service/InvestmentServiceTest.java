package br.com.bancointer.msinvestments.service;

import br.com.bancointer.msinvestments.domain.exception.company.CompanyNotFoundException;
import br.com.bancointer.msinvestments.domain.exception.customer.CustomerNotFoundException;
import br.com.bancointer.msinvestments.domain.exception.investment.InsuficientTotalToInvestException;
import br.com.bancointer.msinvestments.domain.exception.investment.InvestmentNotFoundException;
import br.com.bancointer.msinvestments.repository.CompanyRepository;
import br.com.bancointer.msinvestments.repository.CustomerRepository;
import br.com.bancointer.msinvestments.repository.InvestmentRepository;
import br.com.bancointer.msinvestments.service.impl.InvestmentServiceImpl;
import br.com.bancointer.msinvestments.test.TestUtils;
import br.com.bancointer.msinvestments.util.PageUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class InvestmentServiceTest {

    @Mock
    private InvestmentRepository investmentRepository;

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private CompanyRepository companyRepository;

    @InjectMocks
    private InvestmentServiceImpl investmentService;

    // Section: SAVE
    @Test
    @DisplayName("Quando o método save() é chamado com um InvestmentRequest válido, então deve retornar a InvestmentResponse criado." +
            "When the method save() is called with a valid InvestmentRequest, then it should be return the InvestmentResponse created.")
    void save_success() throws InvestmentNotFoundException, CompanyNotFoundException, CustomerNotFoundException, InsuficientTotalToInvestException {
        var investment = TestUtils.getSampleInvestmentWithId(1L);
        var investmentRequest = TestUtils.getSampleInvestmentRequest(investment);
        var actualInvestmentResponse = TestUtils.getSampleInvestmentResponse(investment);
        var investmentList = List.of(investment);
        when(customerRepository.findByCpf(investmentRequest.getCustomerCpf())).thenReturn(Optional.of(investment.getCustomer()));
        when(companyRepository.findAllByStatus(any(Boolean.class))).thenReturn(List.of(investment.getCompany()));
        when(investmentRepository.saveAll(anyList())).thenReturn(investmentList);
        var expectedInvestmentSaved = investmentService.save(investmentRequest);
        assertThat(actualInvestmentResponse, is(equalTo(expectedInvestmentSaved)));
    }

    @Test
    @DisplayName("Quando o método save() é chamado com um InvestmentRequest válido e não existem empresas da qual o cliente nunca investiu, então deve" +
            "iniciar com o SemiRandomDiversifier e retornar a InvestmentResponse criado." +
            "When the method save() is called with a valid InvestmentRequest and there are no companies that the client has never invested in, so" +
            "start with SemiRandomDiversifier and, then it should be return the InvestmentResponse created.")
    void save_successStartingWithSemiRandomDiversifier() throws CompanyNotFoundException, InvestmentNotFoundException, CustomerNotFoundException, InsuficientTotalToInvestException {
        var customer = TestUtils.getSampleCustomerWithId(1L);
        var investments = TestUtils.getInvestmentListOfAllCompanies(customer);
        customer.setInvestments(investments);
        var investmentRequest = TestUtils.getPersonalizedInvestmentRequest("97568.51", 50);
        var actualInvestmentResponse = TestUtils.getInvestmentResponseWithTwoInvestments();
        when(customerRepository.findByCpf(customer.getCpf())).thenReturn(Optional.of(customer));
        when(companyRepository.findAllByStatus(any(Boolean.class))).thenReturn(TestUtils.getListOfAllCompanies());
        when(investmentRepository.saveAll(anyList())).thenReturn(actualInvestmentResponse.getInvestmentsMade());
        var expectedInvestmentSaved = investmentService.save(investmentRequest);
        assertThat(actualInvestmentResponse.getInvestmentsMade(), is(equalTo(expectedInvestmentSaved.getInvestmentsMade())));
    }

    @Test
    @DisplayName("Quando o método save() é chamado com um InvestmentRequest válido e não existem empresas da qual o cliente nunca investiu, então deve" +
            "iniciar com o SemiRandomDiversifier e retornar a InvestmentResponse criado." +
            "When the method save() is called with a valid InvestmentRequest and there are no companies that the client has never invested in, so" +
            "start with SemiRandomDiversifier and, then it should be return the InvestmentResponse created.")
    void save_successStartingWithSemiRandomDiversifier2() throws CompanyNotFoundException, InvestmentNotFoundException, CustomerNotFoundException, InsuficientTotalToInvestException {
        var customer = TestUtils.getSampleCustomerWithId(1L);
        var investments = TestUtils.getInvestmentListOfSomeCompanies(customer);
        customer.setInvestments(investments);
        var investmentRequest = TestUtils.getPersonalizedInvestmentRequest("57.14", 7);
        var actualInvestmentResponse = TestUtils.getInvestmentResponseWithTwoInvestments();
        when(customerRepository.findByCpf(customer.getCpf())).thenReturn(Optional.of(customer));
        when(companyRepository.findAllByStatus(any(Boolean.class))).thenReturn(TestUtils.getListOfAllCompanies());
        when(investmentRepository.saveAll(anyList())).thenReturn(actualInvestmentResponse.getInvestmentsMade());
        var expectedInvestmentSaved = investmentService.save(investmentRequest);
        assertThat(actualInvestmentResponse.getInvestmentsMade(), is(equalTo(expectedInvestmentSaved.getInvestmentsMade())));
    }

    @Test
    @DisplayName("Quando o método save() é chamado com um CPF do cliente inválido, então uma exceção deve ser lançada." +
            "When the method save() is called with a invalid client CPF, then a exception should be thrown.")
    void save_fail() {
        var invalidInvestmentRequest = TestUtils.getInvestmentRequestWithInvalidCpf();
        assertThrows(CustomerNotFoundException.class, () -> investmentService.save(invalidInvestmentRequest));
    }

    @Test
    @DisplayName("Quando o método save() é chamado, mas não há empresas possíveis para investir, então uma exceção deve ser lançada." +
            "When the method save() is called, but there are no possible companies to invest, then a exception should be thrown.")
    void save_failDueToInsuficientTotalToInvestException() {
        var investment = TestUtils.getSampleInvestmentWithId(1L);
        investment.getCompany().setStockPrice(new BigDecimal(1500));
        var investmentRequest = TestUtils.getSampleInvestmentRequest(investment);
        when(customerRepository.findByCpf(investmentRequest.getCustomerCpf())).thenReturn(Optional.of(investment.getCustomer()));
        when(companyRepository.findAllByStatus(any(Boolean.class))).thenReturn(List.of(investment.getCompany()));
        var investmentPersonalized = TestUtils.getPersonalizedInvestment(TestUtils.getEmptyCustomer(), TestUtils.getEmptyCompany());
        assertNotEquals(investment, investmentPersonalized);
        assertThrows(InsuficientTotalToInvestException.class, () -> investmentService.save(investmentRequest));
    }

    // Section: FIND ALL
    @Test
    @DisplayName("Quando o método findAllByCustomerIdAndCompanyId() é chamado com os parâmetros válidos (ex. Pageable, customerId, companyId), então deve retornar um Page<Investment>." +
            "When the method findAllByCustomerIdAndCompanyId() is called with a valid paramenters (i.e. Pageable, customerId, companyId), then it should be return the Page<Investment>.")
    void findAllByCustomerIdAndCompanyId_success() throws InvestmentNotFoundException {
        var actualInvestmentPage = TestUtils.getSamplePageInvestmentWithId(TestUtils.getSampleInvestmentWithId(1L));
        when(investmentRepository.findAllByCustomerIdAndCompanyId(any(Pageable.class), any(Long.class), any(Long.class))).thenReturn(actualInvestmentPage);
        var expectedInvestmentSaved = investmentService.findAllByCustomerIdAndCompanyId(PageUtils.getDefaultPageRequest(), 1L, 1L);
        assertThat(actualInvestmentPage, is(equalTo(expectedInvestmentSaved)));
    }

    @Test
    @DisplayName("Quando o método findAllByCustomerIdAndCompanyId(), mas nenhum investimento é encontrado, então uma exceção deve ser lançada." +
            "When the method findAllByCustomerIdAndCompanyId() , but no investment was found, then a exception should be thrown.")
    void findAllByCustomerIdAndCompanyId_fail() {
        when(investmentRepository.findAllByCustomerIdAndCompanyId(any(Pageable.class), any(Long.class), any(Long.class))).thenReturn(Page.empty());
        assertThrows(InvestmentNotFoundException.class, () -> investmentService.findAllByCustomerIdAndCompanyId(PageUtils.getDefaultPageRequest(), 1L, 1L));
    }

    @Test
    @DisplayName("Quando o método findAllByCustomerId() é chamado com os parâmetros válidos (ex. Pageable, customerId, companyId), então deve retornar um Page<Investment>." +
            "When the method findAllByCustomerId() is called with a valid paramenters (i.e. Pageable, customerId, companyId), then it should be return the Page<Investment>.")
    void findAllByCustomerId_success() throws InvestmentNotFoundException {
        var actualInvestmentPage = TestUtils.getSamplePageInvestmentWithId(TestUtils.getSampleInvestmentWithId(1L));
        when(investmentRepository.findAllByCustomerId(any(Pageable.class), any(Long.class))).thenReturn(actualInvestmentPage);
        var expectedInvestmentSaved = investmentService.findAllByCustomerIdAndCompanyId(PageUtils.getDefaultPageRequest(), 1L, null);
        assertThat(actualInvestmentPage, is(equalTo(expectedInvestmentSaved)));
    }

    @Test
    @DisplayName("Quando o método findAllByCompanyId() é chamado com os parâmetros válidos (ex. Pageable, customerId, companyId), então deve retornar um Page<Investment>." +
            "When the method findAllByCompanyId() is called with a valid paramenters (i.e. Pageable, customerId, companyId), then it should be return the Page<Investment>.")
    void findAllByCompanyId_success() throws InvestmentNotFoundException {
        var actualInvestmentPage = TestUtils.getSamplePageInvestmentWithId(TestUtils.getSampleInvestmentWithId(1L));
        when(investmentRepository.findAllByCompanyId(any(Pageable.class), any(Long.class))).thenReturn(actualInvestmentPage);
        var expectedInvestmentSaved = investmentService.findAllByCustomerIdAndCompanyId(PageUtils.getDefaultPageRequest(), null, 1L);
        assertThat(actualInvestmentPage, is(equalTo(expectedInvestmentSaved)));
    }

    @Test
    @DisplayName("Quando o método findAll() é chamado com os parâmetros válidos (ex. Pageable, customerId, companyId), então deve retornar um Page<Investment>." +
            "When the method findAll() is called with a valid paramenters (i.e. Pageable, customerId, companyId), then it should be return the Page<Investment>.")
    void findAll_success() throws InvestmentNotFoundException {
        var actualInvestmentPage = TestUtils.getSamplePageInvestmentWithId(TestUtils.getSampleInvestmentWithId(1L));
        when(investmentRepository.findAll(any(Pageable.class))).thenReturn(actualInvestmentPage);
        var expectedInvestmentSaved = investmentService.findAllByCustomerIdAndCompanyId(PageUtils.getDefaultPageRequest(), null, null);
        assertThat(actualInvestmentPage, is(equalTo(expectedInvestmentSaved)));
    }

    // Section: FIND BY
    @Test
    @DisplayName("Quando o método findById() é chamado com um ID válido, então deve retornar o investimento." +
            "When the method findById() is called with a valid ID, then it should be return the investment.")
    void findById_success() throws InvestmentNotFoundException {
        var actualInvestment = TestUtils.getSampleInvestmentWithId(1L);
        System.out.println(actualInvestment.hashCode());
        when(investmentRepository.findById(any(Long.class))).thenReturn(Optional.of(actualInvestment));
        var expectedInvestment = investmentService.findById(1L);
        assertThat(actualInvestment, is(equalTo(expectedInvestment)));
    }

    @Test
    @DisplayName("Quando o método findById() é chamado com um ID inválido, então uma exceção deve ser lançada." +
            "When the method findById() is called with a invalid ID, then a exception should be thrown.")
    void findById_fail() {
        when(investmentRepository.findById(any(Long.class))).thenReturn(Optional.empty());
        assertThrows(InvestmentNotFoundException.class, () -> investmentService.findById(1L));
    }
}
