package br.com.bancointer.msinvestments.test;

public class TestConstants {

    public static final String API_V1_COMPANY = "/api/v1/company";
    public static final String API_V1_COMPANY_1 = "/api/v1/company/1";
    public static final String API_V1_COMPANY_ACTIVE = "/api/v1/company/active";
    public static final String API_V1_COMPANY_1_ACTIVE = "/api/v1/company/1/active";
    public static final String API_V1_COMPANY_1_INACTIVE = "/api/v1/company/1/inactive";

    public static final String API_V1_CUSTOMER = "/api/v1/customer";
    public static final String API_V1_CUSTOMER_1 = "/api/v1/customer/1";

    public static final String API_V1_INVESTMENT = "/api/v1/investment";
    public static final String API_V1_INVESTMENT_1 = "/api/v1/investment/1";

    public final static String NON_EXISTENT_TICKER = "TEST999";
    public final static String INVALID_CPF = "000.000.000-00";
    public final static String VALID_CPF = "584.412.420-03";
}
