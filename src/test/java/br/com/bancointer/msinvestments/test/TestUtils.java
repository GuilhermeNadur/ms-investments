package br.com.bancointer.msinvestments.test;

import br.com.bancointer.msinvestments.domain.Company;
import br.com.bancointer.msinvestments.domain.Customer;
import br.com.bancointer.msinvestments.domain.Investment;
import br.com.bancointer.msinvestments.domain.dto.CompanyDTO;
import br.com.bancointer.msinvestments.domain.dto.CustomerDTO;
import br.com.bancointer.msinvestments.domain.dto.request.InvestmentRequest;
import br.com.bancointer.msinvestments.domain.dto.response.InvestmentResponse;
import br.com.bancointer.msinvestments.util.PageUtils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TestUtils {

    // Section: INVESTMENT
    public static Page<Investment> getSamplePageInvestmentWithId(Investment investment) {
        return getSamplePageInvestmentWithId(PageUtils.getDefaultPageRequest(), investment);
    }

    public static Page<Investment> getSamplePageInvestmentWithId(Pageable pageable, Investment investment) {
        return new PageImpl<>(Collections.singletonList(investment), pageable, 1L);
    }

    public static List<Investment> getInvestmentListOfAllCompanies(Customer customer) {
        return Arrays.asList(
                getPersonalizedInvestment(1L, customer, getBancoInterCompany()),
                getPersonalizedInvestment(2L, customer, getMagazineLuizaCompany()),
                getPersonalizedInvestment(3L, customer, getSulAmericaCompany()),
                getPersonalizedInvestment(4L, customer, getEngieCompany()),
                getPersonalizedInvestment(5L, customer, getCVCBrasilCompany()),
                getPersonalizedInvestment(6L, customer, getLojasRennerCompany()),
                getPersonalizedInvestment(7L, customer, getMarisaCompany())
        );
    }

    public static List<Investment> getInvestmentListOfSomeCompanies(Customer customer) {
        return Arrays.asList(
                getPersonalizedInvestment(2L, customer, getMagazineLuizaCompany()),
                getPersonalizedInvestment(3L, customer, getSulAmericaCompany()),
                getPersonalizedInvestment(4L, customer, getEngieCompany()),
                getPersonalizedInvestment(7L, customer, getMarisaCompany())
        );
    }

    private static Investment getPersonalizedInvestment(Long id, Customer customer, Company company) {
        return Investment.builder()
                .id(id)
                .customer(customer)
                .company(company)
                .numberOfShares(1)
                .total(company.getStockPrice())
                .change(BigDecimal.ZERO)
                .build();
    }

    public static InvestmentResponse getInvestmentResponseWithTwoInvestments() {
        return InvestmentResponse.builder()
                .investmentsMade(List.of(getSampleInvestmentWithId(1L), getSampleInvestmentWithId(1L)))
                .amount(new BigDecimal("100"))
                .change(BigDecimal.ZERO)
                .build();
    }

    public static InvestmentResponse getSampleInvestmentResponse(Investment investment) {
        return InvestmentResponse.builder()
                .investmentsMade(Collections.singletonList(investment))
                .amount(investment.getTotal())
                .change(BigDecimal.ONE)
                .build();
    }

    public static InvestmentRequest getInvestmentRequestWithInvalidCpf() {
        return InvestmentRequest.builder()
                .customerCpf(TestConstants.INVALID_CPF)
                .totalToInvest(BigDecimal.TEN)
                .numberOfCompaniesToDiversify(1)
                .build();
    }

    public static InvestmentRequest getPersonalizedInvestmentRequest(String totalToInvest, Integer numberOfCompaniesToDiversify) {
        return InvestmentRequest.builder()
                .customerCpf(TestConstants.VALID_CPF)
                .totalToInvest(new BigDecimal(totalToInvest))
                .numberOfCompaniesToDiversify(numberOfCompaniesToDiversify)
                .build();
    }

    public static InvestmentRequest getSampleInvestmentRequest(Investment investment) {
        return InvestmentRequest.builder()
                .customerCpf(investment.getCustomer().getCpf())
                .totalToInvest(investment.getTotal())
                .numberOfCompaniesToDiversify(1)
                .build();
    }

    public static Investment getPersonalizedInvestment(Customer customer, Company company) {
        return Investment.builder()
                .customer(customer)
                .company(company)
                .numberOfShares(1)
                .total(company.getStockPrice())
                .change(BigDecimal.ZERO)
                .build();
    }

    public static Investment getSampleInvestmentWithId(Long id) {
        return Investment.builder()
                .id(id)
                .customer(getSampleCustomerWithId(id))
                .company(getSampleCompanyWithId(id))
                .numberOfShares(1)
                .total(getSampleCompanyWithId(id).getStockPrice())
                .change(BigDecimal.ONE)
                .build();
    }

    // Section: CUSTOMER
    public static Page<Customer> getSamplePageCustomer() {
        return getSamplePageCustomer(PageUtils.getDefaultPageRequest());
    }

    public static Page<Customer> getSamplePageCustomer(Pageable pageable) {
        return new PageImpl<>(Collections.singletonList(getSampleCustomerWithId(1L)), pageable, 1L);
    }

    public static CustomerDTO getSampleCustomerDTO() {
        return CustomerDTO.builder()
                .name("SampleCompanyDTO")
                .cpf(TestConstants.VALID_CPF)
                .investments(new ArrayList<>())
                .build();
    }

    public static Customer getEmptyCustomer() {
        return Customer.builder().build();
    }

    public static Customer getSampleCustomerWithId(Long id) {
        return Customer.builder()
                .id(id)
                .name("SampleCustomerWithId")
                .cpf(TestConstants.VALID_CPF)
                .investments(new ArrayList<>())
                .build();
    }

    public static Customer getSampleCustomerWithoutId() {
        return Customer.builder()
                .name("SampleCustomerWithoutId")
                .cpf(TestConstants.VALID_CPF)
                .investments(new ArrayList<>())
                .build();
    }

    // Section: COMPANY
    public static Page<Company> getSamplePageCompany() {
        return getSamplePageCompany(PageUtils.getDefaultPageRequest());
    }

    public static Page<Company> getSamplePageCompany(Pageable pageable) {
        return new PageImpl<>(Collections.singletonList(getSampleCompanyWithId(1L)), pageable, 1L);
    }

    public static CompanyDTO getSampleCompanyDTO() {
        return CompanyDTO.builder()
                .name("SampleCompanyDTO")
                .ticker("TEST01")
                .stockPrice(BigDecimal.TEN)
                .status(true)
                .build();
    }

    public static List<Company> getListOfAllCompanies() {
        return Arrays.asList(
                getBancoInterCompany(),
                getMagazineLuizaCompany(),
                getSulAmericaCompany(),
                getEngieCompany(),
                getCVCBrasilCompany(),
                getLojasRennerCompany(),
                getMarisaCompany()
        );
    }

    public static Company getEmptyCompany() {
        return Company.builder().build();
    }

    public static Company getBancoInterCompany() {
        return Company.builder()
                .id(1L)
                .name("Banco Inter")
                .ticker("BIDI11")
                .stockPrice(new BigDecimal("66.51"))
                .status(true)
                .build();
    }

    public static Company getMagazineLuizaCompany() {
        return Company.builder()
                .id(2L)
                .name("Magazine Luiza")
                .ticker("MGLU3")
                .stockPrice(new BigDecimal("66.51"))
                .status(true)
                .build();
    }

    public static Company getSulAmericaCompany() {
        return Company.builder()
                .id(3L)
                .name("Sul América")
                .ticker("SULA11")
                .stockPrice(new BigDecimal("28.26"))
                .status(true)
                .build();
    }

    public static Company getEngieCompany() {
        return Company.builder()
                .id(4L)
                .name("Engie")
                .ticker("EGIE3")
                .stockPrice(new BigDecimal("38.30"))
                .status(true)
                .build();
    }

    public static Company getCVCBrasilCompany() {
        return Company.builder()
                .id(5L)
                .name("CVC Brasil")
                .ticker("CVCB3")
                .stockPrice(new BigDecimal("20.87"))
                .status(true)
                .build();
    }

    public static Company getLojasRennerCompany() {
        return Company.builder()
                .id(6L)
                .name("Lojas Renner")
                .ticker("LREN3")
                .stockPrice(new BigDecimal("36.95"))
                .status(true)
                .build();
    }

    public static Company getMarisaCompany() {
        return Company.builder()
                .id(7L)
                .name("Marisa")
                .ticker("AMAR3")
                .stockPrice(new BigDecimal("66.51"))
                .status(true)
                .build();
    }

    public static Company getSampleInactiveCompanyWithoutId() {
        return Company.builder()
                .name("SampleCompanyWithId")
                .ticker("TEST01")
                .stockPrice(BigDecimal.TEN)
                .status(false)
                .build();
    }

    public static Company getSampleInactiveCompanyWithId(Long id) {
        return Company.builder()
                .id(id)
                .name("SampleCompanyWithId")
                .ticker("TEST01")
                .stockPrice(BigDecimal.TEN)
                .status(false)
                .build();
    }

    public static Company getSampleCompanyWithId(Long id) {
        return Company.builder()
                .id(id)
                .name("SampleCompanyWithId")
                .ticker("TEST01")
                .stockPrice(BigDecimal.TEN)
                .status(true)
                .build();
    }

    public static Company getSampleCompanyWithoutId() {
        return Company.builder()
                .name("SampleCompanyWithoutId")
                .ticker("TEST02")
                .stockPrice(BigDecimal.TEN)
                .status(true)
                .build();
    }

    // Section: JSON
    public static String convertToJSON(Object object) {
        try {
            return new ObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                    .registerModules(new JavaTimeModule())
                    .writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
