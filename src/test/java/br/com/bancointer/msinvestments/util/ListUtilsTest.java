package br.com.bancointer.msinvestments.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest(classes = { ListUtils.class })
public class ListUtilsTest {

    @Test
    @DisplayName("Quando o método getMiddleElement() é chamado com uma lista válida, então deve retornar o elemento do meio." +
            "When the method getMiddleElement() is called with a valid list, then it should be return the middle element.")
    void getMiddleElement_middleElement() {
        assertThat("MIDDLE", is(equalTo(ListUtils.getMiddleElement(List.of("FIRST", "MIDDLE", "LAST")))));
    }

    @Test
    @DisplayName("Quando o método getMiddleElement() é chamado com uma lista de único elemento, então deve retornar tal elemento." +
            "When the method getMiddleElement() is called with a single element list, then it should be return such element.")
    void getMiddleElement_uniqueElement() {
        assertThat("FIRST", is(equalTo(ListUtils.getMiddleElement(List.of("FIRST")))));
    }

    @Test
    @DisplayName("Quando o método getMiddleElement() é chamado com uma lista vazia, então deve retornar null." +
            "When the method getMiddleElement() is called with a empty list, then it should be return null.")
    void getMiddleElement_null() {
        assertNull(ListUtils.getMiddleElement(Collections.emptyList()));
    }

    @Test
    @DisplayName("Quando o método getElementByPosition() é chamado com um parâmetro inválido, então deve retornar null." +
            "When the method getElementByPosition() is called with a invalid parameter, then it should be return null.")
    void getElementByPosition_null() {
        assertNull(ListUtils.getElementByPosition(List.of("FIRST"), "INVALID"));
    }
}
